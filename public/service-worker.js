importScripts('https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: "AIzaSyCaSXK_rcPNjmMyryw92DWfAMiiKwl0HnY",
  authDomain: "acricana-5d993.firebaseapp.com",
  projectId: "acricana-5d993",
  storageBucket: "acricana-5d993.appspot.com",
  messagingSenderId: "540233012587",
  appId: "1:540233012587:web:23575dbb684cb1fe46e82e"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
    icon: '/images/logo2.png'
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});