const staticDevCoffee = "dev-coffee-site-v1"
const assets = [
  
  "/index.php",
  "/app-assets/vendors/css/vendors.min.css",
  "/app-assets/js/core/app.js",
  "/app-assets/js/core/app-menu.js",
  "/app-assets/js/scripts/components.js"
  
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticDevCoffee).then(cache => {
      cache.addAll(assets)
    })
  )
})


self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
      caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)
      })
    )
  })