<?php

namespace App\Controller;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\NoEntitiesConfiguredException;
use JavierEguiluz\Bundle\EasyAdminBundle\Exception\UndefinedEntityException;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use App\Entity\Archivos;


/**
 * Description of AdminController
 *
 * @author Kelvins Insua
 */
class AdminController extends BaseAdminController {

    public function createNewUserEntity() {
        return $this->get('fos_user.user_manager')->createUser();
    }

    public function prePersistUserEntity($user) {
        $user->setUsername($user->getEmail());
        
        $template = 'email/acricana-bienvenido.html.twig';
        if($user->getBusinessUnits()[0]->getId() == 1){
            $template = 'email/als-bienvenido.html.twig';
        }
        $message = (new \Swift_Message())
                    ->setSubject('Bienvenido')
                    ->setFrom('noresponder@comunicados.acricana.org.ar')
                    ->setTo($user->getEmail())
                    ->setBody(
                    $this->container->get('templating')->render(
                            $template , array(
                            'base_url' => getenv('front_end_point'),
                            'user' => $user,
                            )
                    ), 'text/html'
            );
            $this->container->get('mailer')->send($message);
            $this->get('fos_user.user_manager')->updateUser($user, false);
        
    }
    public function preUpdateUserEntity($user) {
        $this->get('fos_user.user_manager')->updateUser($user, false);
    }

    
    public function prePersistComunicadoEntity($comunicado) {
        $businessUnits = $this->getUser()->getBusinessUnitsIds();
        if(in_array(2, $businessUnits)){
            $comunicado->setAprobado(1);
        }else{
            if(in_array('ROLE_DIRECTOR',$this->getUser()->getRoles())){
                $comunicado->setAprobado(1);
            }else if(in_array('ROLE_PRECEPTOR',$this->getUser()->getRoles())){
                $comunicado->setAprobado(1);
            }else{
                $comunicado->setAprobado(1);
            }
            
        }

        $comunicado->setLeido(false);
        $comunicado->setFromAdmin(true);
        
        $comunicado->setCreador($this->getUser());
        if($this->request->files != null && $this->request->files->get("comunicado") != null && $this->request->files->get("comunicado")['archivos'] != null){
            $this->em->persist($comunicado);
            foreach($this->request->files->get("comunicado")['archivos'] as $file){
                
                $a = new Archivos();
                $a->setNombre($file->getClientOriginalName());
                $a->setImageFile($file);
                $a->setComunicado($comunicado);
                $this->em->persist($a);
            }
        }
        
        $this->em->flush();
        if($comunicado->getAprobado()){
            
            $users = $this->em->getRepository('App:User')->createQueryBuilder('u')
            ->where("u.roles LIKE '%ROLE_PADRE%'")
            ->andWhere('u.fcm IS NOT NULL')
            ->getQuery()
            ->getResult();
            
            foreach($users as $u){
                $send = false;
                if($comunicado->getTodos()){
                    $send = true;
                }else{
                    $send = $this->sendToUser($comunicado,$u);
                }
                if($send){
                    $template = 'email/acricana-notificacion.html.twig';
                    if($u->getBusinessUnits()[0]->getId() == 1){
                        $template = 'email/als-notificacion.html.twig';
                    }
                    $message = (new \Swift_Message())
                                ->setSubject('Nueva notificacion')
                                ->setFrom('noresponder@comunicados.acricana.org.ar')
                                ->setTo($u->getEmail())
                                ->setBody(
                                $this->container->get('templating')->render(
                                        $template , array(
                                        'base_url' => getenv('front_end_point'),
                                        'user' => $u,
                                        )
                                ), 'text/html'
                        );
                        $this->container->get('mailer')->send($message);
                    $title = $comunicado->getBusinessUnit()->getNombre();
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $fields = array(
                        'registration_ids' => $u->getFcm(),
        //                    'registration_ids' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                        //'notification' => [, 'color' => "#000", "vibrate" => 300, 'sound' => "default"],
                        'data' => [
                            "body" => $comunicado->getTitulo(), 
                            'title' => 'Tienes un nuevo comunicado',
                            'type' => 'nuevo_comunicado', 
                            'dataId' => $comunicado->getId()
                            ]
                    );
                    $headers = array(
                        'Authorization:key=AAAAfchllWs:APA91bH5K0Pcy8xTZe6owPJfS7SKezDCH47EHkSuF2jVcDx7cW_oSBrvmKwYUjIjrUd4LJcjVvGmb8acqruekfKVRX8EC9qY1mj_IQiFW4rViIX9DzlMa-kqqGL7KVycg1BCnltld5L9',
                        'Content-Type: application/json'
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    $result = curl_exec($ch);
                    if ($result === FALSE) {
                        die('Curl failed: ' . curl_error($ch));
                    }
                    curl_close($ch);
                }
                
        }
        
        }

        $destinatarios = '';

        foreach($comunicado->getTodosCiclos() as $c){
            $comunicado->removeTodosCiclo($c);
        }

        if(count($comunicado->getAlumnos()) > 0){
            $destinatarios = $destinatarios . 'Alumnos: ';
            for($i = 0; $i < count($comunicado->getAlumnos()); $i++){
                $a = $comunicado->getAlumnos()[$i];
                if($a->getCurso()){
                    $comunicado->addTodosCiclo($a->getCurso()->getCiclo());
                }
                if($i == (count($comunicado->getAlumnos())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre() . ' ' . $a->getApellido().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() . ' ' . $a->getApellido().', ';
                }
            }
        }

        if(count($comunicado->getCursos()) > 0){
            $destinatarios = $destinatarios . 'Cursos: ';
            for($i = 0; $i < count($comunicado->getCursos()); $i++){
                $a = $comunicado->getCursos()[$i];
                $comunicado->addTodosCiclo($a->getCiclo());
                
                if($i == (count($comunicado->getCursos())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() .', ';
                }
            }
        }

        if(count($comunicado->getDivisiones()) > 0){
            $destinatarios = $destinatarios . 'Divisiones: ';
            for($i = 0; $i < count($comunicado->getDivisiones()); $i++){
                $a = $comunicado->getDivisiones()[$i];
                if($i == (count($comunicado->getDivisiones())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() .', ';
                }
            }
        }


        if(count($comunicado->getCiclos()) > 0){
            $destinatarios = $destinatarios . 'Ciclos: ';
            for($i = 0; $i < count($comunicado->getCiclos()); $i++){
                $a = $comunicado->getCiclos()[$i];
                $comunicado->addTodosCiclo($a);
                if($i == (count($comunicado->getCiclos())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() .', ';
                }
            }
        }

        $comunicado->setDestinatarios($destinatarios);
    }

    public function sendToUser($comunicado,$u){
        $send = false;
        if(count($comunicado->getAlumnos())){
            foreach($u->getAlumnos() as $a){
                if($comunicado->getAlumnos()->contains($a)){
                    $send = true;
                }
            }
        }

        if(count($comunicado->getCursos())){
            foreach($u->getAlumnos() as $a){
                foreach($a->getCursos() as $c){
                    if($comunicado->getCursos()->contains($c)){
                        $send = true;
                    }
                }                            
            }
        }

        if(count($comunicado->getDivisiones())){
            foreach($u->getAlumnos() as $a){
                foreach($a->getCursos() as $c){
                    if($comunicado->getDivisiones()->contains($c->getDivision())){
                        $send = true;
                    }
                }                            
            }
        }

        if(count($comunicado->getCiclos())){
            foreach($u->getAlumnos() as $a){
                foreach($a->getCursos() as $c){
                    if($comunicado->getCiclos()->contains($c->getCiclo())){
                        $send = true;
                    }
                }                            
            }
        }

        return $send;
    }

    public function preUpdateComunicadoEntity($comunicado) {
        /* dump($this->request->files->get("comunicado")['archivos']);
                die(); */
        if($this->request->files != null && $this->request->files->get("comunicado") != null && $this->request->files->get("comunicado")['archivos'] != null){
            $this->em->persist($comunicado);
            foreach($this->request->files->get("comunicado")['archivos'] as $file){
                
                $a = new Archivos();
                $a->setNombre($file->getClientOriginalName());
                $a->setImageFile($file);
                $a->setComunicado($comunicado);
                $this->em->persist($a);
            }
        }else{
            $archivos = $this->em->getRepository('App:Archivos')->findBy(['comunicado' => $comunicado->getId()]);
            foreach($archivos as $a){
                $a->setComunicado($comunicado);
                $this->em->persist($a);
            }
        }
        
        $this->em->flush();

        $destinatarios = '';

        foreach($comunicado->getTodosCiclos() as $c){
            $comunicado->removeTodosCiclo($c);
        }

        if(count($comunicado->getAlumnos()) > 0){
            $destinatarios = $destinatarios . 'Alumnos: ';
            for($i = 0; $i < count($comunicado->getAlumnos()); $i++){
                $a = $comunicado->getAlumnos()[$i];
                if($a->getCurso()){
                    $comunicado->addTodosCiclo($a->getCurso()->getCiclo());
                }
                if($i == (count($comunicado->getAlumnos())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre() . ' ' . $a->getApellido().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() . ' ' . $a->getApellido().', ';
                }
            }
        }

        if(count($comunicado->getCursos()) > 0){
            $destinatarios = $destinatarios . 'Cursos: ';
            for($i = 0; $i < count($comunicado->getCursos()); $i++){
                $a = $comunicado->getCursos()[$i];
                $comunicado->addTodosCiclo($a->getCiclo());
                
                if($i == (count($comunicado->getCursos())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() .', ';
                }
            }
        }

        if(count($comunicado->getDivisiones()) > 0){
            $destinatarios = $destinatarios . 'Divisiones: ';
            for($i = 0; $i < count($comunicado->getDivisiones()); $i++){
                $a = $comunicado->getDivisiones()[$i];
                if($i == (count($comunicado->getDivisiones())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() .', ';
                }
            }
        }


        if(count($comunicado->getCiclos()) > 0){
            $destinatarios = $destinatarios . 'Ciclos: ';
            for($i = 0; $i < count($comunicado->getCiclos()); $i++){
                $a = $comunicado->getCiclos()[$i];
                $comunicado->addTodosCiclo($a);
                if($i == (count($comunicado->getCiclos())-1) ){
                    $destinatarios =   $destinatarios . $a->getNombre().'. ';
                }else{
                    $destinatarios =   $destinatarios . $a->getNombre() .', ';
                }
            }
        }

        $comunicado->setDestinatarios($destinatarios);
    }


    public function prePersistMensajeEntity($comunicado) {
        $businessUnits = $this->getUser()->getBusinessUnitsIds();
        $parentId = $this->request->query->get('parentId');
        if(in_array(2, $businessUnits)){
            $comunicado->setAprobado(1);
        }else{
            if(in_array('ROLE_MAESTRO',$this->getUser()->getRoles())){
                $comunicado->setAprobado(1);
            }else{
                $comunicado->setAprobado(1);
            }
            
        }

        $parent = $this->em->getRepository('App:Comunicado')->find($parentId);
        $parent->setRespuestasNoLeidas($parent->getRespuestasNoLeidas() + 1);
        $comunicado->setLeido(false);
        $comunicado->setTodos(false);
        $comunicado->setTitulo($parent->getTitulo());
        $comunicado->setBusinessUnit($parent->getBusinessUnit());
        $comunicado->setFromAdmin(true);
        $comunicado->setTipo('consulta');
        $comunicado->setSeccion('mensajes');
        $comunicado->setCreador($this->getUser());
        $comunicado->setParentId($parentId);
        if($this->request->files != null && $this->request->files->get("comunicado") != null && $this->request->files->get("comunicado")['archivos'] != null){
            $this->em->persist($comunicado);
            foreach($this->request->files->get("comunicado")['archivos'] as $file){
                
                $a = new Archivos();
                $a->setNombre($file->getClientOriginalName());
                $a->setImageFile($file);
                $a->setComunicado($comunicado);
                $this->em->persist($a);
            }
        }
        
        $this->em->flush();

            if($comunicado->getAprobado()){

                $template = 'email/acricana-notificacion.html.twig';
                    if($parent->getCreador()->getBusinessUnits()[0]->getId() == 1){
                        $template = 'email/als-notificacion.html.twig';
                    }
                    $message = (new \Swift_Message())
                                ->setSubject('Nueva notificacion')
                                ->setFrom('noresponder@comunicados.acricana.org.ar')
                                ->setTo($parent->getCreador()->getEmail())
                                ->setBody(
                                $this->container->get('templating')->render(
                                        $template , array(
                                        'base_url' => getenv('front_end_point'),
                                        'user' => $parent->getCreador(),
                                        )
                                ), 'text/html'
                        );
                        $this->container->get('mailer')->send($message);


                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array(
                    'registration_ids' => $parent->getCreador()->getFcm(),
    //                    'registration_ids' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                    //'notification' => [, 'color' => "#000", "vibrate" => 300, 'sound' => "default"],
                    'data' => [
                        "body" => $comunicado->getTitulo(), 
                        'title' => 'Tienes un nuevo mensaje',
                        'type' => 'nuevo_mensaje', 
                        'dataId' => $comunicado->getId()
                        ]
                );
                $headers = array(
                    'Authorization:key=AAAAfchllWs:APA91bH5K0Pcy8xTZe6owPJfS7SKezDCH47EHkSuF2jVcDx7cW_oSBrvmKwYUjIjrUd4LJcjVvGmb8acqruekfKVRX8EC9qY1mj_IQiFW4rViIX9DzlMa-kqqGL7KVycg1BCnltld5L9',
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }
                curl_close($ch);
            }
                
        
    }

    public function preUpdateMensajeEntity($comunicado) {
        /* dump($this->request->files->get("comunicado")['archivos']);
                die(); */
        if($this->request->files != null && $this->request->files->get("comunicado") != null && $this->request->files->get("comunicado")['archivos'] != null){
            $this->em->persist($comunicado);
            foreach($this->request->files->get("comunicado")['archivos'] as $file){
                
                $a = new Archivos();
                $a->setNombre($file->getClientOriginalName());
                $a->setImageFile($file);
                $a->setComunicado($comunicado);
                $this->em->persist($a);
            }
        }else{
            $archivos = $this->em->getRepository('App:Archivos')->findBy(['comunicado' => $comunicado->getId()]);
            foreach($archivos as $a){
                $a->setComunicado($comunicado);
                $this->em->persist($a);
            }
        }
        
        $this->em->flush();
    }


    /**
     * The method that is executed when the user performs a 'delete' action to
     * remove any entity.
     *
     * @return RedirectResponse
     */
    protected function deleteAction() {
        $this->dispatch(EasyAdminEvents::PRE_DELETE);

        if ('delete' !== $this->request->get('action')) {
            return $this->redirect($this->generateUrl('easyadmin', array('action' => 'list', 'entity' => $this->entity['name'])));
        }

        $id = $this->request->query->get('id');
        $form = $this->createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);


        $easyadmin = $this->request->attributes->get('easyadmin');
        $entity = $easyadmin['item'];

        $this->dispatch(EasyAdminEvents::PRE_REMOVE, array('entity' => $entity));

            $this->executeDynamicMethod('preRemove<EntityName>Entity', array($entity));

        $this->em->remove($entity);
        $this->em->flush();

        $this->dispatch(EasyAdminEvents::POST_REMOVE, array('entity' => $entity));


        $refererUrl = $this->request->query->get('referer', '');

        $this->dispatch(EasyAdminEvents::POST_DELETE);

        return !empty($refererUrl) ? $this->redirect(urldecode($refererUrl)) : $this->redirect($this->generateUrl('easyadmin', array('action' => 'list', 'entity' => $this->entity['name'])));
    }

    protected function initialize(Request $request) {
        $this->get('translator')->setLocale('es');
        //date_default_timezone_set('America/New_York');
        parent::initialize($request);
    }

    protected function createSearchQueryBuilder($entityClass, $searchQuery, array $searchableFields, $sortField = null, $sortDirection = null, $dqlFilter = null)
    {
        $qb = $this->get('easyadmin.query_builder')->createSearchQueryBuilder($this->entity, $searchQuery, $sortField, $sortDirection, $dqlFilter);
        if($entityClass == "App\Entity\User"){
            $qb->innerJoin('entity.businessUnits', 'b')
                ->andWhere('b.id IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Ciclo"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Division"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Curso"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Alumno"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Materia"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }
        if($this->request->query->get('entity') == "Comunicado" && $this->getUser() != null){
            if(in_array('ROLE_MAESTRO',$this->getUser()->getRoles())){
                $qb->andWhere('entity.fromAdmin = 1')
                ->andWhere("entity.seccion = 'comunicados'")
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere('entity.parentId IS NULL')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            }else if(in_array('ROLE_PRECEPTOR',$this->getUser()->getRoles())){
                $qb->leftJoin('entity.ciclos', 'ci')
                ->leftJoin('entity.divisiones', 'd')
                ->leftJoin('entity.cursos', 'cu');
                $cursosIds = [];
            $divisionesIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCursos() as $c){
                $cursosIds[] = $c->getId();
                $divisionesIds[] = $c->getDivision()->getId();
                $ciclosIds[] = $c->getCiclo()->getId();
                
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            if(count($cursosIds) > 0){
                $orX->add($qb->expr()->in('cu.id', $cursosIds));
            }

            if(count($divisionesIds) > 0){
                $orX->add($qb->expr()->in('d.id', $divisionesIds));
            }

            if(count($ciclosIds) > 0){
                $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            }
            
            
            
            $qb->andWhere($orX);
            }else if(in_array('ROLE_DIRECTOR',$this->getUser()->getRoles())){
                $qb->leftJoin('entity.ciclos', 'ci');
    
                    $cursosIds = [];
                $ciclosIds = [];
    
                foreach($this->getUser()->getCiclos() as $c){
                    $ciclosIds[] = $c->getId();
                    foreach($c->getCursos() as $cu){
                        $cursosIds[] = $cu->getId();
                    }
                }
    
                $orX = $qb->expr()->orX();
                $orX->add($qb->expr()->eq('entity.todos', 1));
                $orX->add($qb->expr()->in('ci.id', $ciclosIds));
                
                $qb->andWhere('entity.fromAdmin = 1')
                ->andWhere($orX)
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere('entity.parentId IS NULL')
                ->andWhere("entity.seccion = 'comunicados'")
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
                $qb->andWhere($orX);
            }else{
                $orX = $qb->expr()->orX();
                $orX->add($qb->expr()->eq('entity.aprobado', 1));
                $orX->add($qb->expr()->eq('entity.aprobado', 2));
                $qb->andWhere('entity.fromAdmin = 1')
                ->andWhere($orX)
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere('entity.parentId IS NULL')
                ->andWhere("entity.seccion = 'comunicados'")
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            }
            
        }

        if($this->request->query->get('entity') == "ComunicadoPA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        if($this->request->query->get('entity') == "ComunicadoRA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NOT NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        if($this->request->query->get('entity') == "Mensaje" && $this->getUser() != null){
            if(in_array('ROLE_MAESTRO',$this->getUser()->getRoles())){
                $qb->andWhere('entity.docente = :docente')
                ->andWhere('entity.parentId IS NULL')
                ->setParameter('docente',$this->getUser()->getId());
            }else if(in_array('ROLE_PRECEPTOR',$this->getUser()->getRoles())){
                $qb->leftJoin('entity.ciclos', 'ci')
                ->leftJoin('entity.divisiones', 'd')
                ->leftJoin('entity.cursos', 'cu');
                $cursosIds = [];
            $divisionesIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCursos() as $c){
                $cursosIds[] = $c->getId();
                $divisionesIds[] = $c->getDivision()->getId();
                $ciclosIds[] = $c->getCiclo()->getId();
                
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            if(count($cursosIds) > 0){
                $orX->add($qb->expr()->in('cu.id', $cursosIds));
            }

            if(count($divisionesIds) > 0){
                $orX->add($qb->expr()->in('d.id', $divisionesIds));
            }

            if(count($ciclosIds) > 0){
                $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            }
            
            
            
            $qb->andWhere($orX);
        }else if(in_array('ROLE_DIRECTOR',$this->getUser()->getRoles())){
            $qb->leftJoin('entity.ciclos', 'ci');

                $cursosIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCiclos() as $c){
                $ciclosIds[] = $c->getId();
                foreach($c->getCursos() as $cu){
                    $cursosIds[] = $cu->getId();
                }
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            
            $qb->andWhere('entity.fromAdmin = 1')
            ->andWhere($orX)
            ->andWhere('entity.businessUnit IN (:ids)')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            $qb->andWhere($orX);
        }else{
                $orX = $qb->expr()->orX();
                $orX->add($qb->expr()->eq('entity.aprobado', 1));
                $orX->add($qb->expr()->eq('entity.aprobado', 2));
                $qb->andWhere('entity.fromAdmin = 0')
                ->andWhere($orX)
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere("entity.seccion = 'mensajes'")
                ->andWhere('entity.parentId IS NULL')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            }
            
        }

        if($this->request->query->get('entity') == "MensajePA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'mensajes'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        if($this->request->query->get('entity') == "MensajeRA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NOT NULL')
            ->andWhere("entity.seccion = 'mensajes'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        return $qb;
        return $qb;
    }

    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        
        /* if($entityClass == "App\Entity\User"){
            if (null === $dqlFilter) {
                $dqlFilter = sprintf('entity.businessUnits IN ('.$this->getUser()->getBusinessUnitsIds().")");
               
            } else {
                //$dqlFilter .= sprintf(' AND entity.user = %s AND entity.fecha = '.$date.$query, $this->getUser()->getId());
            }
        } */

        $qb = parent::createListQueryBuilder($entityClass, $sortDirection, $sortField, $dqlFilter);
        if($entityClass == "App\Entity\User"){
            $qb->innerJoin('entity.businessUnits', 'b')
                ->andWhere('b.id IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Ciclo"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds())
                ->andWhere('entity.id in (:cids)')
                    ->setParameter('cids',$this->getUser()->getCiclosIds());
        }

        if($entityClass == "App\Entity\Division"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Curso"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
            ->join('entity.ciclo', 'ci', 'WITH', 'entity.ciclo = ci.id')
                    ->andWhere('ci.id IN (:cids)')
                    ->setParameter('cids',$this->getUser()->getCiclosIds())
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Alumno"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
            ->join('entity.cursos', 'cu')
                    ->join('cu.ciclo', 'ci', 'WITH', 'cu.ciclo = ci.id')
                    ->andWhere('ci.id IN (:cids)')
                    ->setParameter('cids',$this->getUser()->getCiclosIds())
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }

        if($entityClass == "App\Entity\Materia"){
            $qb->andWhere('entity.businessUnit IN (:ids)')
            ->join('entity.curso', 'cu', 'WITH', 'entity.curso = cu.id')
                    ->join('cu.ciclo', 'ci', 'WITH', 'cu.ciclo = ci.id')
                    ->andWhere('ci.id IN (:cids)')
                    ->setParameter('cids',$this->getUser()->getCiclosIds())
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());
        }
        if($this->request->query->get('entity') == "Comunicado" && $this->getUser() != null){
            if(in_array('ROLE_MAESTRO',$this->getUser()->getRoles())){
                $qb->andWhere('entity.fromAdmin = 1')
                ->andWhere("entity.seccion = 'comunicados'")
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere('entity.parentId IS NULL')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            }else if(in_array('ROLE_PRECEPTOR',$this->getUser()->getRoles())){
                $qb->leftJoin('entity.ciclos', 'ci')
                ->leftJoin('entity.divisiones', 'd')
                ->leftJoin('entity.cursos', 'cu');
            
            $cursosIds = [];
            $divisionesIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCursos() as $c){
                $cursosIds[] = $c->getId();
                $divisionesIds[] = $c->getDivision()->getId();
                $ciclosIds[] = $c->getCiclo()->getId();
                
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            if(count($cursosIds) > 0){
                $orX->add($qb->expr()->in('cu.id', $cursosIds));
            }

            if(count($divisionesIds) > 0){
                $orX->add($qb->expr()->in('d.id', $divisionesIds));
            }

            if(count($ciclosIds) > 0){
                $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            }
            
            
            
            $qb->andWhere($orX);
            }else if(in_array('ROLE_DIRECTOR',$this->getUser()->getRoles())){
                $qb->leftJoin('entity.ciclos', 'ci');

                $cursosIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCiclos() as $c){
                $ciclosIds[] = $c->getId();
                foreach($c->getCursos() as $cu){
                    $cursosIds[] = $cu->getId();
                }
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            
            $qb->andWhere('entity.fromAdmin = 1')
            ->andWhere($orX)
            ->andWhere('entity.businessUnit IN (:ids)')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            $qb->andWhere($orX);
            }else{
                $orX = $qb->expr()->orX();
                $orX->add($qb->expr()->eq('entity.aprobado', 1));
                $orX->add($qb->expr()->eq('entity.aprobado', 2));
                $qb->andWhere('entity.fromAdmin = 1')
                ->andWhere($orX)
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere('entity.parentId IS NULL')
                ->andWhere("entity.seccion = 'comunicados'")
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            }
            
        }

        if($this->request->query->get('entity') == "ComunicadoPA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        if($this->request->query->get('entity') == "ComunicadoRA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NOT NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        if($this->request->query->get('entity') == "Mensaje" && $this->getUser() != null){
            if(in_array('ROLE_MAESTRO',$this->getUser()->getRoles())){
                $qb->andWhere('entity.fromAdmin = 0')
                ->andWhere('entity.docente = :docente')
                ->andWhere('entity.aprobado = 1')
                ->andWhere("entity.seccion = 'mensajes'")
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere('entity.parentId IS NULL')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds())
                ->setParameter('docente',$this->getUser()->getId());
            }else if(in_array('ROLE_PRECEPTOR',$this->getUser()->getRoles())){
                $qb->leftJoin('entity.ciclos', 'ci')
                ->leftJoin('entity.divisiones', 'd')
                ->leftJoin('entity.cursos', 'cu');
                $cursosIds = [];
            $divisionesIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCursos() as $c){
                $cursosIds[] = $c->getId();
                $divisionesIds[] = $c->getDivision()->getId();
                $ciclosIds[] = $c->getCiclo()->getId();
                
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            if(count($cursosIds) > 0){
                $orX->add($qb->expr()->in('cu.id', $cursosIds));
            }

            if(count($divisionesIds) > 0){
                $orX->add($qb->expr()->in('d.id', $divisionesIds));
            }

            if(count($ciclosIds) > 0){
                $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            }
            
            
            
            $qb->andWhere($orX);
        }else if(in_array('ROLE_DIRECTOR',$this->getUser()->getRoles())){
            $qb->leftJoin('entity.ciclos', 'ci');

                $cursosIds = [];
            $ciclosIds = [];

            foreach($this->getUser()->getCiclos() as $c){
                $ciclosIds[] = $c->getId();
                foreach($c->getCursos() as $cu){
                    $cursosIds[] = $cu->getId();
                }
            }

            $orX = $qb->expr()->orX();
            $orX->add($qb->expr()->eq('entity.todos', 1));
            $orX->add($qb->expr()->in('ci.id', $ciclosIds));
            
            $qb->andWhere('entity.fromAdmin = 1')
            ->andWhere($orX)
            ->andWhere('entity.businessUnit IN (:ids)')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'comunicados'")
            ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            $qb->andWhere($orX);
        }else{
                $orX = $qb->expr()->orX();
                $orX->add($qb->expr()->eq('entity.aprobado', 1));
                $orX->add($qb->expr()->eq('entity.aprobado', 2));
                $qb->andWhere('entity.fromAdmin = 0')
                ->andWhere($orX)
                ->andWhere('entity.businessUnit IN (:ids)')
                ->andWhere("entity.seccion = 'mensajes'")
                ->andWhere('entity.parentId IS NULL')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
            }
            
        }

        if($this->request->query->get('entity') == "MensajePA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NULL')
            ->andWhere("entity.seccion = 'mensajes'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        if($this->request->query->get('entity') == "MensajeRA" && $this->getUser() != null){
            $qb->andWhere('entity.aprobado = 0')
            ->andWhere('entity.parentId IS NOT NULL')
            ->andWhere("entity.seccion = 'mensajes'")
            ->andWhere('entity.businessUnit IN (:ids)')
                ->setParameter('ids',$this->getUser()->getBusinessUnitsIds());;
        }

        return $qb;
    }

    public function aprobarAction()
    {
        $id = $this->request->query->get('id');
        $comunicado = $this->em->getRepository('App:Comunicado')->find($id);
        $comunicado->setAprobado(1);
        $this->em->flush();
        if($comunicado->getSeccion() == 'comunicados'){
            $users = $this->em->getRepository('App:User')->createQueryBuilder('u')
            ->where("u.roles LIKE '%ROLE_PADRE%'")
            ->andWhere('u.fcm IS NOT NULL')
            ->getQuery()
            ->getResult();
            
            foreach($users as $u){
                $send = false;
                if($comunicado->getTodos()){
                    $send = true;
                }else{
                    $send = $this->sendToUser($comunicado,$u);
                }
                if($send){

                    $template = 'email/acricana-notificacion.html.twig';
                    if($u->getBusinessUnits()[0]->getId() == 1){
                        $template = 'email/als-notificacion.html.twig';
                    }
                    $message = (new \Swift_Message())
                                ->setSubject('Nueva notificacion')
                                ->setFrom('noresponder@comunicados.acricana.org.ar')
                                ->setTo($u->getEmail())
                                ->setBody(
                                $this->container->get('templating')->render(
                                        $template , array(
                                        'base_url' => getenv('front_end_point'),
                                        'user' => $u,
                                        )
                                ), 'text/html'
                        );
                        $this->container->get('mailer')->send($message);


                    $title = $comunicado->getBusinessUnit()->getNombre();
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $fields = array(
                        'registration_ids' => $u->getFcm(),
        //                    'registration_ids' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                        //'notification' => [, 'color' => "#000", "vibrate" => 300, 'sound' => "default"],
                        'data' => [
                            "body" => $comunicado->getTitulo(), 
                            'title' => 'Tienes un nuevo comunicado',
                            'type' => 'nuevo_comunicado', 
                            'dataId' => $comunicado->getId()
                            ]
                    );
                    $headers = array(
                        'Authorization:key=AAAAfchllWs:APA91bH5K0Pcy8xTZe6owPJfS7SKezDCH47EHkSuF2jVcDx7cW_oSBrvmKwYUjIjrUd4LJcjVvGmb8acqruekfKVRX8EC9qY1mj_IQiFW4rViIX9DzlMa-kqqGL7KVycg1BCnltld5L9',
                        'Content-Type: application/json'
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    $result = curl_exec($ch);
                    if ($result === FALSE) {
                        die('Curl failed: ' . curl_error($ch));
                    }
                    curl_close($ch);
                }
                
        }
        }else{
            if($comunicado->getParentId()){
                $parent = $this->em->getRepository('App:Comunicado')->find($comunicado->getParentId());
                $template = 'email/acricana-notificacion.html.twig';
                    if($parent->getCreador()->getBusinessUnits()[0]->getId() == 1){
                        $template = 'email/als-notificacion.html.twig';
                    }
                    $message = (new \Swift_Message())
                                ->setSubject('Nueva notificacion')
                                ->setFrom('noresponder@comunicados.acricana.org.ar')
                                ->setTo($parent->getCreador()->getEmail())
                                ->setBody(
                                $this->container->get('templating')->render(
                                        $template , array(
                                        'base_url' => getenv('front_end_point'),
                                        'user' => $parent->getCreador(),
                                        )
                                ), 'text/html'
                        );
                        $this->container->get('mailer')->send($message);
                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array(
                    'registration_ids' => $parent->getCreador()->getFcm(),
    //                    'registration_ids' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                    //'notification' => [, 'color' => "#000", "vibrate" => 300, 'sound' => "default"],
                    'data' => [
                        "body" => $comunicado->getTitulo(), 
                        'title' => 'Tienes un nuevo mensaje',
                        'type' => 'nuevo_mensaje', 
                        'dataId' => $comunicado->getId()
                        ]
                );
                $headers = array(
                    'Authorization:key=AAAAfchllWs:APA91bH5K0Pcy8xTZe6owPJfS7SKezDCH47EHkSuF2jVcDx7cW_oSBrvmKwYUjIjrUd4LJcjVvGmb8acqruekfKVRX8EC9qY1mj_IQiFW4rViIX9DzlMa-kqqGL7KVycg1BCnltld5L9',
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }
                curl_close($ch);
            }
            
            
        }
        

        // redirect to the 'list' view of the given entity
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }
    public function rechazarAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository('App:Comunicado')->find($id);
        $entity->setAprobado(2);
        $this->em->flush();

        // redirect to the 'list' view of the given entity
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }

    public function verRespuestasAction()
    {
        $id = $this->request->query->get('id');
        $c = $this->em->getRepository('App:Comunicado')->find($id);
        if(in_array('ROLE_MAESTRO',$this->getUser()->getRoles())){
            $cs = $this->em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->where('c.aprobado = 1')
                    ->andWhere('c.parentId = :parentId')
                    ->setParameter('parentId', $c->getId())
                    ->orderBy('c.id','ASC')
                    ->getQuery()
                    ->getResult();
        }else{
            $cs = $this->em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->andWhere('c.parentId = :parentId')
                    ->setParameter('parentId', $c->getId())
                    ->orderBy('c.id','ASC')
                    ->getQuery()
                    ->getResult();
        }
        $c->setRespuestasNoLeidas(0);
        $c->setRespuestas($cs);
        $this->em->flush();
        // redirect to the 'list' view of the given entity
        return $this->render('easy_admin/respuestas.html.twig', array(
            'comunicado' => $c
        ));
    }

    public function aprobarPanelAction()
    {
        $id = $this->request->query->get('id');
        $comunicado = $this->em->getRepository('App:Comunicado')->find($id);
        $comunicado->setAprobado(1);
        $this->em->flush();

        if($comunicado->getSeccion() == 'comunicados'){
            $users = $this->em->getRepository('App:User')->createQueryBuilder('u')
            ->where("u.roles LIKE '%ROLE_PADRE%'")
            ->andWhere('u.fcm IS NOT NULL')
            ->getQuery()
            ->getResult();
            
            foreach($users as $u){
                $send = false;
                if($comunicado->getTodos()){
                    $send = true;
                }else{
                    $send = $this->sendToUser($comunicado,$u);
                }
                if($send){
                    $template = 'email/acricana-notificacion.html.twig';
                    if($u->getBusinessUnits()[0]->getId() == 1){
                        $template = 'email/als-notificacion.html.twig';
                    }
                    $message = (new \Swift_Message())
                                ->setSubject('Nueva notificacion')
                                ->setFrom('noresponder@comunicados.acricana.org.ar')
                                ->setTo($u->getEmail())
                                ->setBody(
                                $this->container->get('templating')->render(
                                        $template , array(
                                        'base_url' => getenv('front_end_point'),
                                        'user' => $u,
                                        )
                                ), 'text/html'
                        );
                        $this->container->get('mailer')->send($message);
                    $title = $comunicado->getBusinessUnit()->getNombre();
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $fields = array(
                        'registration_ids' => $u->getFcm(),
        //                    'registration_ids' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                        //'notification' => [, 'color' => "#000", "vibrate" => 300, 'sound' => "default"],
                        'data' => [
                            "body" => $comunicado->getTitulo(), 
                            'title' => 'Tienes un nuevo comunicado',
                            'type' => 'nuevo_comunicado', 
                            'dataId' => $comunicado->getId()
                            ]
                    );
                    $headers = array(
                        'Authorization:key=AAAAfchllWs:APA91bH5K0Pcy8xTZe6owPJfS7SKezDCH47EHkSuF2jVcDx7cW_oSBrvmKwYUjIjrUd4LJcjVvGmb8acqruekfKVRX8EC9qY1mj_IQiFW4rViIX9DzlMa-kqqGL7KVycg1BCnltld5L9',
                        'Content-Type: application/json'
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    $result = curl_exec($ch);
                    if ($result === FALSE) {
                        die('Curl failed: ' . curl_error($ch));
                    }
                    curl_close($ch);
                }
                
        }
        }else{
            if($comunicado->getParentId()){
                $parent = $this->em->getRepository('App:Comunicado')->find($comunicado->getParentId());
                $template = 'email/acricana-notificacion.html.twig';
                    if($parent->getCreador()->getBusinessUnits()[0]->getId() == 1){
                        $template = 'email/als-notificacion.html.twig';
                    }
                    $message = (new \Swift_Message())
                                ->setSubject('Nueva notificacion')
                                ->setFrom('noresponder@comunicados.acricana.org.ar')
                                ->setTo($parent->getCreador()->getEmail())
                                ->setBody(
                                $this->container->get('templating')->render(
                                        $template , array(
                                        'base_url' => getenv('front_end_point'),
                                        'user' => $parent->getCreador(),
                                        )
                                ), 'text/html'
                        );
                        $this->container->get('mailer')->send($message);
                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array(
                    'registration_ids' => $parent->getCreador()->getFcm(),
    //                    'registration_ids' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                    //'notification' => [, 'color' => "#000", "vibrate" => 300, 'sound' => "default"],
                    'data' => [
                        "body" => $comunicado->getTitulo(), 
                        'title' => 'Tienes un nuevo mensaje',
                        'type' => 'nuevo_mensaje', 
                        'dataId' => $comunicado->getId()
                        ]
                );
                $headers = array(
                    'Authorization:key=AAAAfchllWs:APA91bH5K0Pcy8xTZe6owPJfS7SKezDCH47EHkSuF2jVcDx7cW_oSBrvmKwYUjIjrUd4LJcjVvGmb8acqruekfKVRX8EC9qY1mj_IQiFW4rViIX9DzlMa-kqqGL7KVycg1BCnltld5L9',
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }
                curl_close($ch);
            }
            
            
        }

        return $this->verPadre($comunicado);
    }
    public function rechazarPanelAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository('App:Comunicado')->find($id);
        $entity->setAprobado(2);
        $this->em->flush();

        return $this->verPadre($entity);
    }

    public function verPadre($child){
        $c = $this->em->getRepository('App:Comunicado')->find($child->getParentId());

        $cs = $this->em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->andWhere('c.parentId = :parentId')
                    ->setParameter('parentId', $c->getId())
                    ->orderBy('c.id','DESC')
                    ->getQuery()
                    ->getResult();
        $c->setRespuestas($cs);

        // redirect to the 'list' view of the given entity
        return $this->render('easy_admin/respuestas.html.twig', array(
            'comunicado' => $c
        ));
    }

    public function responderAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository('App:Comunicado')->find($id);
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'new',
            'entity' => $this->request->query->get('entity'),
            'parentId' => $id
        ));
    }

    public function verLecturasAction()
    {
        $id = $this->request->query->get('id');
        $c = $this->em->getRepository('App:Comunicado')->find($id); 

        // redirect to the 'list' view of the given entity
        return $this->render('easy_admin/lecturas.html.twig', array(
            'comunicado' => $c
        ));
    }

    
    
    }
    

