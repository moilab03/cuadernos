<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlumnoRepository")
 */
class Alumno
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apellido;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="alumnos")
     */
    private $padres;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comentarios;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombreContacto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apellidoContacto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefonoContacto;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comentarioContacto;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Curso", inversedBy="alumnos")
     */
    private $cursos;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessUnit", inversedBy="ciclos")
     */
    private $businessUnit;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Comunicado", mappedBy="alumnos")
     */
    private $comunicados;

    public function __construct()
    {
        $this->padres = new ArrayCollection();
        $this->cursos = new ArrayCollection();
        $this->comunicados = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getPadres(): Collection
    {
        return $this->padres;
    }

    public function addPadre(User $padre): self
    {
        if (!$this->padres->contains($padre)) {
            $this->padres[] = $padre;
        }

        return $this;
    }

    public function removePadre(User $padre): self
    {
        if ($this->padres->contains($padre)) {
            $this->padres->removeElement($padre);
        }

        return $this;
    }

    public function __toString(){
        return $this->getNombre()." ".$this->getApellido();
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getComentarios(): ?string
    {
        return $this->comentarios;
    }

    public function setComentarios(string $comentarios): self
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    public function getNombreContacto(): ?string
    {
        return $this->nombreContacto;
    }

    public function setNombreContacto(string $nombreContacto): self
    {
        $this->nombreContacto = $nombreContacto;

        return $this;
    }

    public function getApellidoContacto(): ?string
    {
        return $this->apellidoContacto;
    }

    public function setApellidoContacto(string $apellidoContacto): self
    {
        $this->apellidoContacto = $apellidoContacto;

        return $this;
    }

    public function getTelefonoContacto(): ?string
    {
        return $this->telefonoContacto;
    }

    public function setTelefonoContacto(string $telefonoContacto): self
    {
        $this->telefonoContacto = $telefonoContacto;

        return $this;
    }

    public function getComentarioContacto(): ?string
    {
        return $this->comentarioContacto;
    }

    public function setComentarioContacto(?string $comentarioContacto): self
    {
        $this->comentarioContacto = $comentarioContacto;

        return $this;
    }

    
    public function getCursos()
    {
        if($this->cursos){
            return $this->cursos->get(0);
        }else{
            return null;
        }
        
    }

    public function setCursos(Curso $curso): self
    {
        $this->cursos = [];
        $this->cursos[] = $curso;
        return $this;
    }
    
    public function getBusinessUnit(): ?BusinessUnit
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?BusinessUnit $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    public function getCurso(){
        $date = new \DateTime();
        foreach($this->getCursos() as $c){
            if($c->getFechaInicio() <= $date && $c->getFechaFin() >= $date){
                return $c;
            }
        }

        return null;
    }

    /**
     * @return Collection|Comunicado[]
     */
    public function getComunicados(): Collection
    {
        return $this->comunicados;
    }

    public function addComunicado(Comunicado $comunicado): self
    {
        if (!$this->comunicados->contains($comunicado)) {
            $this->comunicados[] = $comunicado;
            $comunicado->addAlumno($this);
        }

        return $this;
    }

    public function removeComunicado(Comunicado $comunicado): self
    {
        if ($this->comunicados->contains($comunicado)) {
            $this->comunicados->removeElement($comunicado);
            $comunicado->removeAlumno($this);
        }

        return $this;
    }
}
