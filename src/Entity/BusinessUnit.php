<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BusinessUnitRepository")
 */
class BusinessUnit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logoUrl;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="businessUnits")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ciclo", mappedBy="businessUnit")
     */
    private $ciclos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comunicado", mappedBy="businessUnit")
     */
    private $comunicados;


    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->ciclos = new ArrayCollection();
        $this->comunicados = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    public function setLogoUrl(string $logoUrl): self
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    public function __toString(){
        return $this->getNombre();
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addBusinessUnit($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeBusinessUnit($this);
        }

        return $this;
    }

    /**
     * @return Collection|Ciclo[]
     */
    public function getCiclos(): Collection
    {
        return $this->ciclos;
    }

    public function addCiclo(Ciclo $ciclo): self
    {
        if (!$this->ciclos->contains($ciclo)) {
            $this->ciclos[] = $ciclo;
            $ciclo->setBusinessUnit($this);
        }

        return $this;
    }

    public function removeCiclo(Ciclo $ciclo): self
    {
        if ($this->ciclos->contains($ciclo)) {
            $this->ciclos->removeElement($ciclo);
            // set the owning side to null (unless already changed)
            if ($ciclo->getBusinessUnit() === $this) {
                $ciclo->setBusinessUnit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comunicado[]
     */
    public function getComunicados(): Collection
    {
        return $this->comunicados;
    }

    public function addComunicado(Comunicado $comunicado): self
    {
        if (!$this->comunicados->contains($comunicado)) {
            $this->comunicados[] = $comunicado;
            $comunicado->setBusinessUnit($this);
        }

        return $this;
    }

    public function removeComunicado(Comunicado $comunicado): self
    {
        if ($this->comunicados->contains($comunicado)) {
            $this->comunicados->removeElement($comunicado);
            // set the owning side to null (unless already changed)
            if ($comunicado->getBusinessUnit() === $this) {
                $comunicado->setBusinessUnit(null);
            }
        }

        return $this;
    }
}
