<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use \Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MateriaRepository")
 * @Vich\Uploadable
 */
class Materia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="materias")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $profesor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $programa;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="programa")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Curso", inversedBy="materias")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $curso;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessUnit", inversedBy="ciclos")
     */
    private $businessUnit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getProfesor(): ?User
    {
        return $this->profesor;
    }

    public function setProfesor(?User $profesor): self
    {
        $this->profesor = $profesor;

        return $this;
    }

    public function getPrograma(): ?string
    {
        return $this->programa;
    }

    public function setPrograma(?string $programa): self
    {
        $this->programa = $programa;

        return $this;
    }

    
     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            $this->updatedAt = new \DateTime('now');
        }
        
    }
    
    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;        
    }


    public function __toString(){
        return $this->getNombre();
    }

    public function getCurso(): ?Curso
    {
        return $this->curso;
    }

    public function setCurso(?Curso $curso): self
    {
        $this->curso = $curso;

        return $this;
    }
    
    public function getBusinessUnit(): ?BusinessUnit
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?BusinessUnit $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }
}
