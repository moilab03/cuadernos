<?php

namespace App\Form\Field;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class BusinessUnitsType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    

    /**
    * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => 'App:BusinessUnit',
            'label' => false,
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('b')
                ->andWhere('b.id IN (:ids)')
                ->setParameter('ids',$this->security->getUser()->getBusinessUnitsIds());
            },
            'attr' => array('data-widget' => 'select2'),
            'multiple' => true,
            'expanded'=> false,
        ]);
    }

    /**
    * @return string|null
    */
    public function getParent()
    {
        return EntityType::class;
    }
}