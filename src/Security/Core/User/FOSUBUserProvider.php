<?php
namespace App\Security\Core\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface as FOSUserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use UserBundle\Entity\Client;
use UserBundle\Entity\MembershipFree;

use HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse;

class FOSUBUserProvider extends BaseClass
{
 private $entityManager;
 private $responseObject;
  public function __construct(UserManagerInterface $userManager, EntityManager $entityManager, array $properties)
    {
        $this->userManager = $userManager;
        $this->entityManager = $entityManager;
        $this->properties  = array_merge($this->properties, $properties);
        $this->accessor    = PropertyAccess::createPropertyAccessor();
        $this->responseObject = new PathUserResponse();
    }
 
    

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $email = $response->getEmail();
        $firstname = $response->getFirstname();
        $lastname = $response->getLastname();
        $attr = $response->getResponse();
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        
        if (null === $user) {
            $user = $this->userManager->findUserByEmail($email);

            $service = $response->getResourceOwner()->getName();
            $setter = 'set' . ucfirst($service);
            $setterId = $setter . 'Id';
            $setterToken = $setter . 'AccessToken';
            $setterName = $setter . 'Name';
            $setterDate = $setter . 'Data';

            if (null === $user) {
                $user = $this->userManager->createUser();
                $user->$setterId($username);
                $user->$setterToken($response->getAccessToken());
                //$user->$setterName($attr['name']);
                //$user->$setterDate($response->getResponse());
                
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setPassword($username);
                $user->setEnabled(true);
                $user->addRole('ROLE_CLIENT');
                $this->userManager->updateUser($user); 
//                $firstname = $request->get("firstname");
//                $lastname = $request->get("lastname");
                $phone = '00';
                $client = new Client();
                $client->setUser($user);
                $client->setFirstName($firstname." GREG");
                $client->setSecondName($lastname." ESCA");
                $client->setPhone($phone);
                $client->setPerfilCompleted(0);                
//                $em = $this->getDoctrine()->getManager();
                $this->entityManager->persist($client);
                $this->entityManager->flush();
                $promotions= $this->entityManager->getRepository('ParametersBundle:Promotion')->findAll();
                
                
                foreach($promotions as $p){
                    if($p->getFinisDayRL()>=(new \DateTime())){
                        $membershipFree = new MembershipFree();
                        $membershipFree->setFreeClasses($p->getQuotas());
                        $membershipFree->setStartDay(new \Datetime());
                        $membershipFree->setFinishDay($p->getFinishDay());
                        $membershipFree->setUser($user);
                        $membershipFree->setIdPromotion($p->getId());
                        $this->entityManager->persist($membershipFree);
                        $this->entityManager->flush();
                    }
                }
                
                

                return $user;
            } else {
                $user->$setterId($username);
                $user->$setterToken($response->getAccessToken());
                //$user->$setterName($attr['name']);
                //$user->$setterDate($response->getResponse());
                
                $this->userManager->updateUser($user);

                return $user;
            }
        }

        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        if (method_exists($user, $setter)) {
            $user->$setter($response->getAccessToken());
        }
    //var_dump($user);
        return $user;
    }
    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->userManager->refreshUser($user);
    }
    
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response){
        $property = $this->getProperty($response);
        $username = $response->getUsername();
 
        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';
 
        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }
 
        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
 
        $this->userManager->updateUser($user);
    }

	/**
	 * Ad-hoc creation of user
	 *
	 * @param UserResponseInterface $response
	 *
	 * @return User
	 */
	protected function createUserByOAuthUserResponse(UserResponseInterface $response)
	{
		$user = $this->userManager->createUser();
		$this->updateUserByOAuthUserResponse($user, $response);

		// set default values taken from OAuth sign-in provider account
		if (null !== $email = $response->getEmail()) {
			$user->setEmail($email);
		}

		if (null === $this->userManager->findUserByUsername($response->getNickname())) {
			$user->setUsername($response->getNickname());
		}

		$user->setEnabled(true);

		return $user;
	}

	/**
	 * Attach OAuth sign-in provider account to existing user
	 *
	 * @param FOSUserInterface      $user
	 * @param UserResponseInterface $response
	 *
	 * @return FOSUserInterface
	 */
	protected function updateUserByOAuthUserResponse(FOSUserInterface $user, UserResponseInterface $response)
	{
		$providerName = $response->getResourceOwner()->getName();
		$providerNameSetter = 'set'.ucfirst($providerName).'Id';
		$user->$providerNameSetter($response->getUsername());

		if(!$user->getPassword()) {
			// generate unique token
			$secret = md5(uniqid(rand(), true));
			$user->setPassword($secret);
		}

		return $user;
	}
}