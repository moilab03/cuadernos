<?php
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-components/src/Knp/Component/Pager/PaginatorInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-components/src/Knp/Component/Pager/Paginator.php';

class Paginator_f262b94 extends \Knp\Component\Pager\Paginator implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Knp\Component\Pager\Paginator|null wrapped object, if the proxy is initialized
     */
    private $valueHolder3f041 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera8feb = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesfd19e = [
        
    ];

    public function setDefaultPaginatorOptions(array $options)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, 'setDefaultPaginatorOptions', array('options' => $options), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        return $this->valueHolder3f041->setDefaultPaginatorOptions($options);
    }

    public function paginate($target, $page = 1, $limit = 10, array $options = [])
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, 'paginate', array('target' => $target, 'page' => $page, 'limit' => $limit, 'options' => $options), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        return $this->valueHolder3f041->paginate($target, $page, $limit, $options);
    }

    public function subscribe(\Symfony\Component\EventDispatcher\EventSubscriberInterface $subscriber)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, 'subscribe', array('subscriber' => $subscriber), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        return $this->valueHolder3f041->subscribe($subscriber);
    }

    public function connect($eventName, $listener, $priority = 0)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, 'connect', array('eventName' => $eventName, 'listener' => $listener, 'priority' => $priority), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        return $this->valueHolder3f041->connect($eventName, $listener, $priority);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        unset($instance->eventDispatcher, $instance->defaultOptions);

        $instance->initializera8feb = $initializer;

        return $instance;
    }

    public function __construct(?\Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher = null)
    {
        static $reflection;

        if (! $this->valueHolder3f041) {
            $reflection = $reflection ?? new \ReflectionClass('Knp\\Component\\Pager\\Paginator');
            $this->valueHolder3f041 = $reflection->newInstanceWithoutConstructor();
        unset($this->eventDispatcher, $this->defaultOptions);

        }

        $this->valueHolder3f041->__construct($eventDispatcher);
    }

    public function & __get($name)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, '__get', ['name' => $name], $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        if (isset(self::$publicPropertiesfd19e[$name])) {
            return $this->valueHolder3f041->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\Paginator');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder3f041;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder3f041;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\Paginator');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder3f041;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder3f041;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, '__isset', array('name' => $name), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\Paginator');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder3f041;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder3f041;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, '__unset', array('name' => $name), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\Paginator');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder3f041;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder3f041;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, '__clone', array(), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        $this->valueHolder3f041 = clone $this->valueHolder3f041;
    }

    public function __sleep()
    {
        $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, '__sleep', array(), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;

        return array('valueHolder3f041');
    }

    public function __wakeup()
    {
        unset($this->eventDispatcher, $this->defaultOptions);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializera8feb = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializera8feb;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera8feb && ($this->initializera8feb->__invoke($valueHolder3f041, $this, 'initializeProxy', array(), $this->initializera8feb) || 1) && $this->valueHolder3f041 = $valueHolder3f041;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder3f041;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder3f041;
    }
}
