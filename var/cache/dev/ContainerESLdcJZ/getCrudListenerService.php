<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'crud_listener' shared autowired service.

include_once \dirname(__DIR__, 4).'/src/EventListener/EntityCrudListener.php';

return $this->privates['crud_listener'] = new \App\EventListener\EntityCrudListener($this);
