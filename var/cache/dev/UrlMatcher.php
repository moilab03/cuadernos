<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [
            [['_route' => 'index', '_controller' => 'App\\Controller\\AppController::login'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'app_app_loginpost', '_controller' => 'App\\Controller\\AppController::loginPost'], null, ['POST' => 0], null, false, false, null],
        ],
        '/select-bu' => [[['_route' => 'select-bu', '_controller' => 'App\\Controller\\AppController::buAction'], null, ['GET' => 0], null, false, false, null]],
        '/dashboard' => [[['_route' => 'dashboard', '_controller' => 'App\\Controller\\AppController::dahsboardAction'], null, ['GET' => 0], null, false, false, null]],
        '/cerrar-session' => [[['_route' => 'cerrar_session_user', '_controller' => 'App\\Controller\\AppController::cerrarSesionAction'], null, ['GET' => 0], null, false, false, null]],
        '/comunicados' => [[['_route' => 'comunicados', '_controller' => 'App\\Controller\\AppController::comunicadosAction'], null, ['GET' => 0], null, false, false, null]],
        '/mensajes' => [[['_route' => 'mensajes', '_controller' => 'App\\Controller\\AppController::mensajesAction'], null, ['GET' => 0], null, false, false, null]],
        '/crear-mensaje' => [[['_route' => 'crear-mensaje', '_controller' => 'App\\Controller\\AppController::cmensajesAction'], null, ['POST' => 0], null, false, false, null]],
        '/recuperar-contrasena' => [
            [['_route' => 'app_app_recoverypass2', '_controller' => 'App\\Controller\\AppController::recoveryPass2'], null, ['POST' => 0], null, false, false, null],
            [['_route' => 'app_app_recoverypass', '_controller' => 'App\\Controller\\AppController::recoveryPass'], null, ['GET' => 0], null, false, false, null],
        ],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/profile' => [[['_route' => 'fos_user_profile_show', '_controller' => 'fos_user.profile.controller:showAction'], null, ['GET' => 0], null, true, false, null]],
        '/profile/edit' => [[['_route' => 'fos_user_profile_edit', '_controller' => 'fos_user.profile.controller:editAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/register' => [[['_route' => 'fos_user_registration_register', '_controller' => 'fos_user.registration.controller:registerAction'], null, ['GET' => 0, 'POST' => 1], null, true, false, null]],
        '/register/check-email' => [[['_route' => 'fos_user_registration_check_email', '_controller' => 'fos_user.registration.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/register/confirmed' => [[['_route' => 'fos_user_registration_confirmed', '_controller' => 'fos_user.registration.controller:confirmedAction'], null, ['GET' => 0], null, false, false, null]],
        '/resetting/request' => [[['_route' => 'fos_user_resetting_request', '_controller' => 'fos_user.resetting.controller:requestAction'], null, ['GET' => 0], null, false, false, null]],
        '/resetting/send-email' => [[['_route' => 'fos_user_resetting_send_email', '_controller' => 'fos_user.resetting.controller:sendEmailAction'], null, ['POST' => 0], null, false, false, null]],
        '/resetting/check-email' => [[['_route' => 'fos_user_resetting_check_email', '_controller' => 'fos_user.resetting.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/profile/change-password' => [[['_route' => 'fos_user_change_password', '_controller' => 'fos_user.change_password.controller:changePasswordAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'fos_user_security_login', '_controller' => 'App\\Controller\\SecurityController::loginAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/login_check' => [[['_route' => 'fos_user_security_check', '_controller' => 'fos_user.security.controller:checkAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/logout' => [[['_route' => 'fos_user_security_logout', '_controller' => 'fos_user.security.controller:logoutAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/api/login_check' => [[['_route' => 'api_login_check'], null, null, null, false, false, null]],
        '/admin' => [
            [['_route' => 'easyadmin', '_controller' => 'App\\Controller\\AdminController::indexAction'], null, null, null, true, false, null],
            [['_route' => 'admin', '_controller' => 'App\\Controller\\AdminController::indexAction'], null, null, null, true, false, null],
        ],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/a(?'
                    .'|lumno/([^/]++)(*:26)'
                    .'|pi(?'
                        .'|(?:/(index)(?:\\.([^/]++))?)?(*:66)'
                        .'|/(?'
                            .'|docs(?:\\.([^/]++))?(*:96)'
                            .'|contexts/(.+)(?:\\.([^/]++))?(*:131)'
                            .'|users(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:165)'
                                .')'
                                .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                    .'|(*:203)'
                                .')'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/re(?'
                    .'|s(?'
                        .'|ponder\\-(?'
                            .'|comunicado/([^/]++)(*:256)'
                            .'|mensaje/([^/]++)(*:280)'
                        .')'
                        .'|etting/reset/([^/]++)(*:310)'
                    .')'
                    .'|cuperar/([^/]++)(*:335)'
                    .'|gister/confirm/([^/]++)(*:366)'
                .')'
                .'|/set\\-token/([^/]++)/([^/]++)(*:404)'
                .'|/c(?'
                    .'|rear\\-lectura/([^/]++)(*:439)'
                    .'|ambiar/([^/]++)(*:462)'
                .')'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:502)'
                    .'|wdt/([^/]++)(*:522)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:568)'
                            .'|router(*:582)'
                            .'|exception(?'
                                .'|(*:602)'
                                .'|\\.css(*:615)'
                            .')'
                        .')'
                        .'|(*:625)'
                    .')'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        26 => [[['_route' => 'alumno', '_controller' => 'App\\Controller\\AppController::alumnoAction'], ['id'], ['GET' => 0], null, false, true, null]],
        66 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        96 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        131 => [[['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null]],
        165 => [
            [['_route' => 'api_users_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_users_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        203 => [
            [['_route' => 'api_users_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_users_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_users_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        256 => [[['_route' => 'r-comunicados', '_controller' => 'App\\Controller\\AppController::rcomunicadosAction'], ['id'], ['POST' => 0], null, false, true, null]],
        280 => [[['_route' => 'r-mensajes', '_controller' => 'App\\Controller\\AppController::rmensajesAction'], ['id'], ['POST' => 0], null, false, true, null]],
        310 => [[['_route' => 'fos_user_resetting_reset', '_controller' => 'fos_user.resetting.controller:resetAction'], ['token'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        335 => [[['_route' => 'app_app_recuperar2', '_controller' => 'App\\Controller\\AppController::recuperar2'], ['token'], ['GET' => 0], null, false, true, null]],
        366 => [[['_route' => 'fos_user_registration_confirm', '_controller' => 'fos_user.registration.controller:confirmAction'], ['token'], ['GET' => 0], null, false, true, null]],
        404 => [[['_route' => 't-comunicados', '_controller' => 'App\\Controller\\AppController::tcomunicadosAction'], ['id', 'token'], ['GET' => 0], null, false, true, null]],
        439 => [[['_route' => 't-lectura', '_controller' => 'App\\Controller\\AppController::lecturaAction'], ['id'], ['GET' => 0], null, false, true, null]],
        462 => [[['_route' => 'app_app_cambiar', '_controller' => 'App\\Controller\\AppController::cambiar'], ['token'], ['POST' => 0], null, false, true, null]],
        502 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        522 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        568 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        582 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        602 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        615 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        625 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
