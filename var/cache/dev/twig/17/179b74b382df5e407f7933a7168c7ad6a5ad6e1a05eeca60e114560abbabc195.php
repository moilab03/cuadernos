<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* easy_admin/respuestas.html.twig */
class __TwigTemplate_bffdb44961c363db645038ed820540468ed3faf2a8d5a2130fbe6f6efe7f094f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'content_header' => [$this, 'block_content_header'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "easy_admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "easy_admin/respuestas.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "easy_admin/respuestas.html.twig"));

        $this->parent = $this->loadTemplate("easy_admin/layout.html.twig", "easy_admin/respuestas.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <section class=\"content-header\">
        ";
        // line 5
        $this->displayBlock('content_header', $context, $blocks);
        // line 8
        echo "    </section>

    <div class=\"panel-body\">
            <ul class=\"list-group\">
                <li class=\"list-group-item\"><strong>Contenido: </strong> ";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 12, $this->source); })()), "contenido", [], "any", false, false, false, 12), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><strong>Creador: </strong> ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 13, $this->source); })()), "creador", [], "any", false, false, false, 13), "nombre", [], "any", false, false, false, 13), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 13, $this->source); })()), "creador", [], "any", false, false, false, 13), "apellido", [], "any", false, false, false, 13), "html", null, true);
        echo "</li>
                <li class=\"list-group-item\"><strong>Fecha: </strong> ";
        // line 14
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 14, $this->source); })()), "fechaCreacion", [], "any", false, false, false, 14), "d/m/Y"), "html", null, true);
        echo "</li>
            </ul>
        </div>
    <h4 style='margin-left: 15px'>Respuestas</h4>
    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 18, $this->source); })()), "respuestas", [], "any", false, false, false, 18));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 19
            echo "        <div class=\"panel panel-primary\" style='margin: 15px'>
        <div class=\"panel-heading\">";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "titulo", [], "any", false, false, false, 20), "html", null, true);
            echo "</div>
        <div class=\"panel-body\">
            <ul class=\"list-group\">
                <li class=\"list-group-item\"><strong>Contenido: </strong> ";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "contenido", [], "any", false, false, false, 23), "html", null, true);
            echo "</li>
                <li class=\"list-group-item\"><strong>Creador: </strong> ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "creador", [], "any", false, false, false, 24), "nombre", [], "any", false, false, false, 24), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "creador", [], "any", false, false, false, 24), "apellido", [], "any", false, false, false, 24), "html", null, true);
            echo "</li>
                <li class=\"list-group-item\"><strong>Fecha: </strong> ";
            // line 25
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "fechaCreacion", [], "any", false, false, false, 25), "d/m/Y"), "html", null, true);
            echo "</li>
                <li class=\"list-group-item\"><strong>Estatus: </strong> 
                ";
            // line 27
            if ((twig_get_attribute($this->env, $this->source, $context["r"], "aprobado", [], "any", false, false, false, 27) == 0)) {
                echo "<span class=\"label label-default\">Pendiente</span>";
            }
            // line 28
            echo "                ";
            if ((twig_get_attribute($this->env, $this->source, $context["r"], "aprobado", [], "any", false, false, false, 28) == 1)) {
                echo "<span class=\"label label-success\">Aprobado</span>";
            }
            // line 29
            echo "                ";
            if ((twig_get_attribute($this->env, $this->source, $context["r"], "aprobado", [], "any", false, false, false, 29) == 2)) {
                echo "<span class=\"label label-danger\">Rechazado</span>";
            }
            // line 30
            echo "                </li>
                
            </ul>
        </div>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        // line 6
        echo "            <h1 class=\"title\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 6, $this->source); })()), "titulo", [], "any", false, false, false, 6), "html", null, true);
        echo "</h1>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "easy_admin/respuestas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 6,  161 => 5,  150 => 36,  139 => 30,  134 => 29,  129 => 28,  125 => 27,  120 => 25,  114 => 24,  110 => 23,  104 => 20,  101 => 19,  97 => 18,  90 => 14,  84 => 13,  80 => 12,  74 => 8,  72 => 5,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"easy_admin/layout.html.twig\" %}

{% block content %}
    <section class=\"content-header\">
        {% block content_header %}
            <h1 class=\"title\">{{comunicado.titulo}}</h1>
        {% endblock content_header %}
    </section>

    <div class=\"panel-body\">
            <ul class=\"list-group\">
                <li class=\"list-group-item\"><strong>Contenido: </strong> {{comunicado.contenido}}</li>
                <li class=\"list-group-item\"><strong>Creador: </strong> {{comunicado.creador.nombre}} {{comunicado.creador.apellido}}</li>
                <li class=\"list-group-item\"><strong>Fecha: </strong> {{comunicado.fechaCreacion|date('d/m/Y')}}</li>
            </ul>
        </div>
    <h4 style='margin-left: 15px'>Respuestas</h4>
    {% for r in comunicado.respuestas %}
        <div class=\"panel panel-primary\" style='margin: 15px'>
        <div class=\"panel-heading\">{{r.titulo}}</div>
        <div class=\"panel-body\">
            <ul class=\"list-group\">
                <li class=\"list-group-item\"><strong>Contenido: </strong> {{r.contenido}}</li>
                <li class=\"list-group-item\"><strong>Creador: </strong> {{r.creador.nombre}} {{r.creador.apellido}}</li>
                <li class=\"list-group-item\"><strong>Fecha: </strong> {{r.fechaCreacion|date('d/m/Y')}}</li>
                <li class=\"list-group-item\"><strong>Estatus: </strong> 
                {% if r.aprobado == 0 %}<span class=\"label label-default\">Pendiente</span>{% endif %}
                {% if r.aprobado == 1 %}<span class=\"label label-success\">Aprobado</span>{% endif %}
                {% if r.aprobado == 2 %}<span class=\"label label-danger\">Rechazado</span>{% endif %}
                </li>
                
            </ul>
        </div>
    </div>
    {% endfor %}
    
{% endblock %}", "easy_admin/respuestas.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/easy_admin/respuestas.html.twig");
    }
}
