<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mensajes.html.twig */
class __TwigTemplate_3e2c50998a47ca72a6ee9d7356595d2b8cc4d19577c29420a70135fb163747e1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "mensajes.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "mensajes.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Mensajes - Acricana</title>
    <link rel=\"manifest\" href=\"../../../manifest.json\" />
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/editors/quill/katex.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/editors/quill/monokai-sublime.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/editors/quill/quill.snow.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/app-email.css\">
    <!-- END: Page CSS-->

      <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js\"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js\"></script>

  <!-- Add Firebase products that you want to use -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js\"></script>
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js\"></script>
  ";
        // line 47
        $context["user"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 47, $this->source); })()), "session", [], "any", false, false, false, 47), "get", [0 => "user"], "method", false, false, false, 47);
        // line 48
        echo "  <script> 
  var userId = ";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 49, $this->source); })()), "id", [], "any", false, false, false, 49), "html", null, true);
        echo "; 
  </script>
 <script src=\"firebase-messaging-sw.js\"></script>

 <!-- JavaScript -->
<script src=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js\"></script>

<!-- CSS -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css\"/>
<!-- Default theme -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css\"/>

<link rel=\"apple-touch-icon\" href=\"images/logo72.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo96.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo128.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo144.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo152.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo192.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo384.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo512.png\" />
<meta name=\"apple-mobile-web-app-status-bar\" content=\"#db4938\" />
<meta name=\"theme-color\" content=\"#db4938\" />

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern dark-layout content-left-sidebar email-application navbar-floating footer-static  \" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"content-left-sidebar\" data-layout=\"dark-layout\">
    ";
        // line 78
        $this->loadTemplate("menu.html.twig", "mensajes.html.twig", 78)->display($context);
        // line 79
        echo "    <div class=\"app-content content\">
      <div class=\"content-overlay\"></div>
      <div class=\"header-navbar-shadow\"></div>
      <div class=\"content-wrapper\">
          <div class=\"content-header row\">
              <div class=\"content-header-left col-md-9 col-12 mb-2\">
                  <div class=\"row breadcrumbs-top\">
                      <div class=\"col-12\">
                          <h2 class=\"content-header-title float-left mb-0\">Mensajes</h2>
                          <div class=\"breadcrumb-wrapper col-12\">
                              <ol class=\"breadcrumb\">
                                  ";
        // line 92
        echo "                                  <!-- <li class=\"breadcrumb-item\"><a href=\"#\">Card</a>
                                  </li>
                                  <li class=\"breadcrumb-item active\">Advance Card
                                  </li> -->
                              </ol>
                          </div>
                      </div>
                  </div>
                  <div class=\"form-group form-group-compose text-center compose-btn\">
                      <button type=\"button\" class=\"btn btn-primary btn-block my-2\" data-toggle=\"modal\" data-target=\"#createForm\"><i class=\"feather icon-edit\"></i> Nuevo mensaje</button>
                  </div>
              </div>
              </div>
              </div>
        <div class=\"content-area-wrapper\">


            <div class=\"sidebar-left\">
                <div class=\"sidebar\">

                    <div class=\"sidebar-content email-app-sidebar d-flex\">
                        <span class=\"sidebar-close-icon\">
                            <i class=\"feather icon-x\"></i>
                        </span>
                        <div class=\"email-app-menu\">
                            <div class=\"form-group form-group-compose text-center compose-btn\">
                                <!-- <button type=\"button\" class=\"btn btn-primary btn-block my-2\" data-toggle=\"modal\" data-target=\"#composeForm\"><i class=\"feather icon-edit\"></i> Compose</button> -->
                            </div>
                            <div class=\"sidebar-menu-list\">
                                <div class=\"list-group list-group-messages font-medium-1\">
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0 pt-0 active\">
                                      <br>
                                        <i class=\"font-medium-5 feather icon-mail mr-50\"></i> Mensajes <span class=\"badge badge-primary badge-pill float-right\"></span>
                                    </a>
                                    <!-- <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 fa fa-paper-plane-o mr-50\"></i> Sent</a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-edit-2 mr-50\"></i> Draft <span class=\"badge badge-warning badge-pill float-right\">4</span> </a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-star mr-50\"></i>
                                        Starred</a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-info mr-50\"></i>
                                        Spam <span class=\"badge badge-danger badge-pill float-right\">3</span> </a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-trash mr-50\"></i>
                                        Trash</a> -->
                                </div>
                                <hr>
                                <h5 class=\"my-2 pt-25\">Tipos de mensajes</h5>
                                <div class=\"list-group list-group-labels font-medium-1\">
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0 d-flex align-items-center\"><span class=\"bullet bullet-danger mr-1\"></span> Consultas</a>
                                    <!-- <a href=\"#\" class=\"list-group-item list-group-item-action border-0 d-flex align-items-center\"><span class=\"bullet bullet-primary mr-1\"></span> CUrs</a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0 d-flex align-items-center\"><span class=\"bullet bullet-warning mr-1\"></span> Important</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class=\"modal fade text-left\" id=\"composeForm\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"emailCompose\" aria-hidden=\"true\">
                        <div class=\"modal-dialog modal-dialog-scrollable\">
                            <form class=\"modal-content\" id=\"c-form\" method=\"POST\" enctype=\"multipart/form-data\">
                                <div class=\"modal-header\">
                                    <h3 class=\"modal-title text-text-bold-600\" id=\"emailCompose\">Respuesta</h3>
                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\">&times;</span>
                                    </button>
                                </div>
                                <div class=\"modal-body pt-1\">
                                    <div id=\"email-container\">
                                        <textarea name=\"contenido\" required minlength=\"10\" style=\"width: 470px;height: 150px;\"></textarea>
                                    </div>
                                    <div class=\"form-group mt-2\">
                                        <div class=\"custom-file\">
                                            <input type=\"file\" class=\"custom-file-input\" id=\"emailAttach\" name=\"file\">
                                            <label class=\"custom-file-label\" for=\"emailAttach\">Archivos</label>
                                        </div>
                                    </div>
                                </div>
                                <input type='hidden' name='alumnoId' value='";
        // line 166
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 166, $this->source); })()), "request", [], "any", false, false, false, 166), "get", [0 => "alumnoId"], "method", false, false, false, 166), "html", null, true);
        echo "' />
                                <div class=\"modal-footer\">
                                    <input type=\"submit\" value=\"Enviar\" class=\"btn btn-primary\">
                                    <input type=\"Reset\" value=\"Cancelar\" class=\"btn btn-white\" data-dismiss=\"modal\">
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class=\"modal fade text-left\" id=\"createForm\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"emailCompose\" aria-hidden=\"true\">
                        <div class=\"modal-dialog modal-dialog-scrollable\">
                            <form class=\"modal-content\" id=\"create-form\" action='/crear-mensaje' method=\"POST\" enctype=\"multipart/form-data\">
                                <div class=\"modal-header\">
                                    <h3 class=\"modal-title text-text-bold-600\" id=\"emailCompose\">Crear mensaje</h3>
                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\">&times;</span>
                                    </button>
                                </div>
                                <div class=\"modal-body pt-1\">
                                <div id=\"email-container\">
                                    <label class=\"label\" for=\"title\">Asunto</label>
                                        <input name=\"titulo\" id='title' required minlength=\"5\" style=\"width: 470px\"/>
                                    </div>
                                    <div id=\"email-container\">
                                        <label class=\"label\" for=\"content\">Contenido</label>
                                        <textarea name=\"contenido\" id='content' required minlength=\"10\" style=\"width: 470px;height: 150px;\"></textarea>
                                    </div>
                                    ";
        // line 194
        $context["bu"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 194, $this->source); })()), "session", [], "any", false, false, false, 194), "get", [0 => "bu"], "method", false, false, false, 194);
        // line 195
        echo "                                    ";
        if (((isset($context["bu"]) || array_key_exists("bu", $context) ? $context["bu"] : (function () { throw new RuntimeError('Variable "bu" does not exist.', 195, $this->source); })()) == "1")) {
            // line 196
            echo "                                        <div id=\"email-container\">
                                        <label class=\"label\" for=\"docente\">Docente</label>
                                        <select required class='form-control' name='docente'>
                                            ";
            // line 199
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["profesores"]) || array_key_exists("profesores", $context) ? $context["profesores"] : (function () { throw new RuntimeError('Variable "profesores" does not exist.', 199, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 200
                echo "                                                <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "id", [], "any", false, false, false, 200), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nombre", [], "any", false, false, false, 200), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "apellido", [], "any", false, false, false, 200), "html", null, true);
                echo "</option>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 202
            echo "                                        </select>
                                    </div>
                                    ";
        }
        // line 205
        echo "                                    
                                    
                                    <div class=\"form-group mt-2\">
                                        <div class=\"custom-file\">
                                            <input type=\"file\" class=\"custom-file-input\" id=\"emailAttach\" name=\"file\">
                                            <label class=\"custom-file-label\" for=\"emailAttach\">Archivos</label>
                                        </div>
                                    </div>

                                    
                                </div>
                                <input type='hidden' name='alumnoId' value='";
        // line 216
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 216, $this->source); })()), "request", [], "any", false, false, false, 216), "get", [0 => "alumnoId"], "method", false, false, false, 216), "html", null, true);
        echo "' />
                                <div class=\"modal-footer\">
                                    <input type=\"submit\" value=\"Enviar\" class=\"btn btn-primary\">
                                    <input type=\"Reset\" value=\"Cancelar\" class=\"btn btn-white\" data-dismiss=\"modal\">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class=\"content-right\">
                <div class=\"content-wrapper\">
                    <div class=\"content-header row\">
                    </div>
                    <div class=\"content-body\">
                        <div class=\"app-content-overlay\"></div>
                        <div class=\"email-app-area\">
                            <!-- Email list Area -->
                            <div class=\"email-app-list-wrapper\">
                                <div class=\"email-app-list\">
                                    <div class=\"app-fixed-search\">
                                        <div class=\"sidebar-toggle d-block d-lg-none\"><i class=\"feather icon-menu\"></i></div>
                                        <fieldset class=\"form-group position-relative has-icon-left m-0\">
                                            <input type=\"text\" class=\"form-control\" id=\"email-search\" placeholder=\"Buscar mensaje\">
                                            <div class=\"form-control-position\">
                                                <i class=\"feather icon-search\"></i>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class=\"email-user-list list-group\">
                                        <ul class=\"users-list-wrapper media-list\">
                                        ";
        // line 249
        $context["logoUrl"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 249, $this->source); })()), "session", [], "any", false, false, false, 249), "get", [0 => "logoUrl"], "method", false, false, false, 249);
        // line 250
        echo "                                        ";
        $context["user"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 250, $this->source); })()), "session", [], "any", false, false, false, 250), "get", [0 => "user"], "method", false, false, false, 250);
        // line 251
        echo "                                        
                                        ";
        // line 252
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comunicados"]) || array_key_exists("comunicados", $context) ? $context["comunicados"] : (function () { throw new RuntimeError('Variable "comunicados" does not exist.', 252, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 253
            echo "                                            <li class=\"media mail";
            if ((twig_get_attribute($this->env, $this->source, $context["c"], "leido", [], "any", false, false, false, 253) == true)) {
                echo "-read";
            }
            echo "\" id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 253), "html", null, true);
            echo "\" onclick='fetch(\"/crear-lectura/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 253), "html", null, true);
            echo "\");\$(\"#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 253), "html", null, true);
            echo "\").addClass(\"mail-read\")'>
                                                <div class=\"media-left pr-50\">
                                                    <div class=\"avatar\">
                                                        <img src=\"";
            // line 256
            echo twig_escape_filter($this->env, (isset($context["logoUrl"]) || array_key_exists("logoUrl", $context) ? $context["logoUrl"] : (function () { throw new RuntimeError('Variable "logoUrl" does not exist.', 256, $this->source); })()), "html", null, true);
            echo "\" alt=\"avtar img holder\">
                                                    </div>

                                                </div>
                                                <div class=\"media-body\">
                                                    <div class=\"user-details\">
                                                        <div class=\"mail-items\">
                                                            <h5 class=\"list-group-item-heading text-bold-600 mb-25\">";
            // line 263
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "titulo", [], "any", false, false, false, 263), "html", null, true);
            echo "</h5>
                                                            <span class=\"list-group-item-text text-truncate\">";
            // line 264
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "tipo", [], "any", false, false, false, 264), "html", null, true);
            echo "</span>
                                                        </div>
                                                        <div class=\"mail-meta-item\">
                                                            <span class=\"float-right\">
                                                                <span class=\"mr-1 bullet ";
            // line 268
            echo (((twig_get_attribute($this->env, $this->source, $context["c"], "tipo", [], "any", false, false, false, 268) == "informativo")) ? ("bullet-success") : ("bullet-warning"));
            echo " bullet-sm\"></span><span class=\"mail-date\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "fechaCreacion", [], "any", false, false, false, 268), "h:i a"), "html", null, true);
            echo "</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class=\"mail-message\">
                                                        <p class=\"list-group-item-text truncate mb-0\">";
            // line 273
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "contenido", [], "any", false, false, false, 273), "html", null, true);
            echo ".</p>
                                                    </div>
                                                </div>
                                            </li>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 278
        echo "                                        </ul>
                                        <div class=\"no-results\">
                                            <h5>No hay mensajes</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ Email list Area -->
                            <!-- Detailed Email View -->
                            ";
        // line 287
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comunicados"]) || array_key_exists("comunicados", $context) ? $context["comunicados"] : (function () { throw new RuntimeError('Variable "comunicados" does not exist.', 287, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 288
            echo "                            <div class=\"email-app-details\" id=\"detalle-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "id", [], "any", false, false, false, 288), "html", null, true);
            echo "\">
                                <div class=\"email-detail-header\">
                                    <div class=\"email-header-left d-flex align-items-center mb-1\">
                                        <span class=\"go-back mr-1\"><i class=\"feather icon-arrow-left font-medium-4\"></i></span>
                                        <h3>";
            // line 292
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "titulo", [], "any", false, false, false, 292), "html", null, true);
            echo "</h3>
                                    </div>
                                    <div class=\"email-header-right mb-1 ml-2 pl-1\">
                                        <ul class=\"list-inline m-0\">
                                            <!-- <li class=\"list-inline-item\"><span class=\"action-icon favorite\"><i class=\"feather icon-star font-medium-5\"></i></span></li> -->
                                            <li class=\"list-inline-item\">
                                                <div class=\"dropdown no-arrow\">
                                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                        <!-- <i class=\"feather icon-folder font-medium-5\"></i> -->
                                                    </a>
                                                    <!-- <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"folder\">
                                                        <a class=\"dropdown-item d-flex font-medium-1\" href=\"#\"><i class=\"font-medium-3 feather icon-edit-2 mr-50\"></i> Draft</a>
                                                        <a class=\"dropdown-item d-flex font-medium-1\" href=\"#\"><i class=\"font-medium-3 feather icon-info mr-50\"></i> Spam</a>
                                                        <a class=\"dropdown-item d-flex font-medium-1\" href=\"#\"><i class=\"font-medium-3 feather icon-trash mr-50\"></i> Trash</a>
                                                    </div> -->
                                                </div>
                                            </li>
                                            <li class=\"list-inline-item\">

                                            </li>
                                            <!-- <li class=\"list-inline-item\"><span class=\"action-icon\"><i class=\"feather icon-mail font-medium-5\"></i></span></li>
                                            <li class=\"list-inline-item\"><span class=\"action-icon\"><i class=\"feather icon-trash font-medium-5\"></i></span></li> -->
                                            ";
            // line 316
            echo "                                        </ul>
                                    </div>
                                </div>
                                <div class=\"email-scroll-area\" style=\"overflow-y: auto;\">
                                    <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"email-label ml-2 my-2 pl-1\">
                                                <span class=\"mr-1 bullet ";
            // line 323
            echo (((twig_get_attribute($this->env, $this->source, $context["c"], "tipo", [], "any", false, false, false, 323) == "informativo")) ? ("bullet-success") : ("bullet-danger"));
            echo " bullet-sm\"></span><small class=\"mail-label\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "tipo", [], "any", false, false, false, 323), "html", null, true);
            echo "</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"card px-1\">
                                                <div class=\"card-header email-detail-head ml-75\">
                                                    <div class=\"user-details d-flex justify-content-between align-items-center flex-wrap\">
                                                        <div class=\"avatar mr-75\">
                                                            <img src=\"";
            // line 333
            echo twig_escape_filter($this->env, (isset($context["logoUrl"]) || array_key_exists("logoUrl", $context) ? $context["logoUrl"] : (function () { throw new RuntimeError('Variable "logoUrl" does not exist.', 333, $this->source); })()), "html", null, true);
            echo "\" alt=\"avtar img holder\" width=\"61\" height=\"61\">
                                                        </div>
                                                        <div class=\"mail-items\">
                                                            <h4 class=\"list-group-item-heading mb-0\">
                                                                ";
            // line 337
            if (twig_in_filter("ROLE_PRECEPTOR", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 337), "roles", [], "any", false, false, false, 337))) {
                // line 338
                echo "                                                                    Preceptoria
                                                                ";
            }
            // line 340
            echo "                                                                ";
            if (twig_in_filter("ROLE_DIRECTOR", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 340), "roles", [], "any", false, false, false, 340))) {
                // line 341
                echo "                                                                    Dirección
                                                                ";
            }
            // line 343
            echo "                                                                ";
            if (twig_in_filter("ROLE_MAESTRO", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 343), "roles", [], "any", false, false, false, 343))) {
                // line 344
                echo "                                                                    Profesor
                                                                ";
            }
            // line 346
            echo "                                                            </h4>
                                                            <div class=\"email-info-dropup dropdown\">
                                                                <span class=\"font-small-3\" id=\"dropdownMenuButton200\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                                    ";
            // line 349
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 349), "nombre", [], "any", false, false, false, 349), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 349), "apellido", [], "any", false, false, false, 349), "html", null, true);
            echo "
                                                                </span>
                                                                <!-- <div class=\"dropdown-menu dropdown-menu-right p-50\" aria-labelledby=\"dropdownMenuButton200\">
                                                                    <div class=\"px-25 dropdown-item\">From: <strong> abaldersong@utexas.edu </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">To: <strong> johndoe@ow.ly </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">Date: <strong> 4:25 AM 13 Jan 2018 </strong></div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"mail-meta-item\">
                                                        <div class=\"mail-time mb-1\">";
            // line 360
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "fechaCreacion", [], "any", false, false, false, 360), "h:i a"), "html", null, true);
            echo "</div>
                                                        <div class=\"mail-date\">";
            // line 361
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "fechaCreacion", [], "any", false, false, false, 361), "d/m/Y"), "html", null, true);
            echo "</div>
                                                    </div>
                                                </div>
                                                <div class=\"card-body mail-message-wrapper pt-2 mb-0\">
                                                    <div class=\"mail-message\" style=\"white-space: pre;\">";
            // line 365
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "contenido", [], "any", false, false, false, 365), "html", null, true);
            echo "</div>
                                                    <div class=\"mail-attachements d-flex\">
                                                        <i class=\"feather icon-paperclip font-medium-5 mr-50\"></i>
                                                        <span>Documentos</span>
                                                    </div>
                                                </div>
                                                <div class=\"mail-files py-2\">
                                                ";
            // line 372
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["c"], "archivos", [], "any", false, false, false, 372));
            foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                // line 373
                echo "                                                    <div class=\"chip chip-primary\">
                                                        <div class=\"chip-body py-50\">
                                                        
                                                            <a class=\"chip-text\" style=\"color: #FFF;\" href=\"/images/";
                // line 376
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "url", [], "any", false, false, false, 376), "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "nombre", [], "any", false, false, false, 376), "html", null, true);
                echo "</a>
                                                            
                                                        </div>
                                                    </div>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 381
            echo "                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    ";
            // line 386
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["c"], "respuestas", [], "any", false, false, false, 386));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 387
                echo "                                        ";
                if (((twig_get_attribute($this->env, $this->source, $context["c"], "aprobado", [], "any", false, false, false, 387) == 1) || ((twig_get_attribute($this->env, $this->source, $context["c"], "aprobado", [], "any", false, false, false, 387) != 1) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 387), "id", [], "any", false, false, false, 387) == twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 387, $this->source); })()), "id", [], "any", false, false, false, 387))))) {
                    // line 388
                    echo "                                        <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"card px-1\">
                                                <div class=\"card-header email-detail-head ml-75\">
                                                    <div class=\"user-details d-flex justify-content-between align-items-center flex-wrap\">
                                                        <div class=\"avatar mr-75\">
                                                            <img src=\"";
                    // line 394
                    echo twig_escape_filter($this->env, (isset($context["logoUrl"]) || array_key_exists("logoUrl", $context) ? $context["logoUrl"] : (function () { throw new RuntimeError('Variable "logoUrl" does not exist.', 394, $this->source); })()), "html", null, true);
                    echo "\" alt=\"avtar img holder\" width=\"61\" height=\"61\">
                                                        </div>
                                                        <div class=\"mail-items\">
                                                            <h4 class=\"list-group-item-heading mb-0\">
                                                                RESPUESTA
                                                            </h4>
                                                            <div class=\"email-info-dropup dropdown\">
                                                                <span class=\"font-small-3\" id=\"dropdownMenuButton200\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                                    ";
                    // line 402
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 402), "nombre", [], "any", false, false, false, 402), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "creador", [], "any", false, false, false, 402), "apellido", [], "any", false, false, false, 402), "html", null, true);
                    echo "
                                                                </span>
                                                                <!-- <div class=\"dropdown-menu dropdown-menu-right p-50\" aria-labelledby=\"dropdownMenuButton200\">
                                                                    <div class=\"px-25 dropdown-item\">From: <strong> abaldersong@utexas.edu </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">To: <strong> johndoe@ow.ly </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">Date: <strong> 4:25 AM 13 Jan 2018 </strong></div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"mail-meta-item\">
                                                        <div class=\"mail-time mb-1\">";
                    // line 413
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "fechaCreacion", [], "any", false, false, false, 413), "h:i a"), "html", null, true);
                    echo "</div>
                                                        <div class=\"mail-date\">";
                    // line 414
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "fechaCreacion", [], "any", false, false, false, 414), "d/m/Y"), "html", null, true);
                    echo "</div>
                                                    </div>
                                                </div>
                                                <div class=\"card-body mail-message-wrapper pt-2 mb-0\">
                                                    <div class=\"mail-message\" style=\"white-space: pre;\">";
                    // line 418
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "contenido", [], "any", false, false, false, 418), "html", null, true);
                    echo "</div>
                                                    <div class=\"mail-attachements d-flex\">
                                                        <i class=\"feather icon-paperclip font-medium-5 mr-50\"></i>
                                                        <span>Documentos</span>
                                                    </div>
                                                </div>
                                                <div class=\"mail-files py-2\">
                                                ";
                    // line 425
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["c"], "archivos", [], "any", false, false, false, 425));
                    foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                        // line 426
                        echo "                                                    <div class=\"chip chip-primary\">
                                                        <div class=\"chip-body py-50\">
                                                        
                                                            <a class=\"chip-text\" style=\"color: #FFF;\" href=\"/images/";
                        // line 429
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "url", [], "any", false, false, false, 429), "html", null, true);
                        echo "\" target=\"_blank\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "nombre", [], "any", false, false, false, 429), "html", null, true);
                        echo "</a>
                                                            
                                                        </div>
                                                    </div>
                                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 434
                    echo "                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    ";
                }
                // line 439
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 440
            echo "


                                        
                                    ";
            // line 444
            if ((twig_get_attribute($this->env, $this->source, $context["c"], "tipo", [], "any", false, false, false, 444) == "consulta")) {
                // line 445
                echo "                                    <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"card\">


                                                <div class=\"card-body\">
                                                    <div class=\"d-flex justify-content-between\">

                                                        <!-- <span class=\"font-medium-1\">El comunicado es de caracter informativo <span class=\"primary cursor-pointer\"><strong>No se puede responder</strong></span> </span>
                                                        <i class=\"feather icon-paperclip font-medium-5 mr-50\"></i> -->
                                                    </div>
                                                    <div class=\"form-group form-group-compose text-center compose-btn\">
                                                        <button type=\"button\" class=\"btn btn-primary btn-block my-2\" data-toggle=\"modal\" data-target=\"#composeForm\"><i class=\"feather icon-edit\"></i> Responder</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    ";
            }
            // line 465
            echo "                                </div>
                            </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 468
        echo "                            <!--/ Detailed Email View -->
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END: Content-->

    <div class=\"sidenav-overlay\"></div>
    <div class=\"drag-target\"></div>

    <!-- BEGIN: Footer-->
    <footer class=\"footer footer-static footer-light\">
        <p class=\"clearfix blue-grey lighten-2 mb-0\"><span class=\"float-md-left d-block d-md-inline-block mt-25\">Acricana - Cuaderno de Comunicados V0.1 - Powered by <a class=\"text-bold-800 grey darken-2\" href=\"#\" target=\"_blank\">Movius</a></span><span class=\"float-md-right d-none d-md-block\">Hand-crafted & Made with<i class=\"feather icon-heart pink\"></i></span>
            <button class=\"btn btn-primary btn-icon scroll-top\" type=\"button\"><i class=\"feather icon-arrow-up\"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/editors/quill/katex.min.js\"></script>
    <script src=\"../../../app-assets/vendors/js/editors/quill/highlight.min.js\"></script>
    <script src=\"../../../app-assets/vendors/js/editors/quill/quill.min.js\"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->
    <script>
    if (\"serviceWorker\" in navigator) {
  window.addEventListener(\"load\", function() {
    navigator.serviceWorker
      .register(\"";
        // line 510
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("sw.js"), "html", null, true);
        echo "\")
      .then(res => console.log(\"service worker registered\"))
      .catch(err => console.log(\"service worker not registered\", err))
      })
    }
    </script>
    <!-- BEGIN: Page JS-->
    <script src=\"../../../app-assets/js/scripts/pages/app-email2.js\"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "mensajes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  750 => 510,  706 => 468,  698 => 465,  676 => 445,  674 => 444,  668 => 440,  662 => 439,  655 => 434,  642 => 429,  637 => 426,  633 => 425,  623 => 418,  616 => 414,  612 => 413,  596 => 402,  585 => 394,  577 => 388,  574 => 387,  570 => 386,  563 => 381,  550 => 376,  545 => 373,  541 => 372,  531 => 365,  524 => 361,  520 => 360,  504 => 349,  499 => 346,  495 => 344,  492 => 343,  488 => 341,  485 => 340,  481 => 338,  479 => 337,  472 => 333,  457 => 323,  448 => 316,  423 => 292,  415 => 288,  411 => 287,  400 => 278,  389 => 273,  379 => 268,  372 => 264,  368 => 263,  358 => 256,  343 => 253,  339 => 252,  336 => 251,  333 => 250,  331 => 249,  295 => 216,  282 => 205,  277 => 202,  264 => 200,  260 => 199,  255 => 196,  252 => 195,  250 => 194,  219 => 166,  143 => 92,  130 => 79,  128 => 78,  96 => 49,  93 => 48,  91 => 47,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Mensajes - Acricana</title>
    <link rel=\"manifest\" href=\"../../../manifest.json\" />
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/editors/quill/katex.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/editors/quill/monokai-sublime.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/editors/quill/quill.snow.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/app-email.css\">
    <!-- END: Page CSS-->

      <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js\"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js\"></script>

  <!-- Add Firebase products that you want to use -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js\"></script>
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js\"></script>
  {% set user = app.session.get('user') %}
  <script> 
  var userId = {{user.id}}; 
  </script>
 <script src=\"firebase-messaging-sw.js\"></script>

 <!-- JavaScript -->
<script src=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js\"></script>

<!-- CSS -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css\"/>
<!-- Default theme -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css\"/>

<link rel=\"apple-touch-icon\" href=\"images/logo72.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo96.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo128.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo144.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo152.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo192.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo384.png\" />
<link rel=\"apple-touch-icon\" href=\"images/logo512.png\" />
<meta name=\"apple-mobile-web-app-status-bar\" content=\"#db4938\" />
<meta name=\"theme-color\" content=\"#db4938\" />

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern dark-layout content-left-sidebar email-application navbar-floating footer-static  \" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"content-left-sidebar\" data-layout=\"dark-layout\">
    {% include 'menu.html.twig' %}
    <div class=\"app-content content\">
      <div class=\"content-overlay\"></div>
      <div class=\"header-navbar-shadow\"></div>
      <div class=\"content-wrapper\">
          <div class=\"content-header row\">
              <div class=\"content-header-left col-md-9 col-12 mb-2\">
                  <div class=\"row breadcrumbs-top\">
                      <div class=\"col-12\">
                          <h2 class=\"content-header-title float-left mb-0\">Mensajes</h2>
                          <div class=\"breadcrumb-wrapper col-12\">
                              <ol class=\"breadcrumb\">
                                  {# <li class=\"breadcrumb-item\"><a href=\"inicio.html\">Tomas Fonzi</a>
                                  </li> #}
                                  <!-- <li class=\"breadcrumb-item\"><a href=\"#\">Card</a>
                                  </li>
                                  <li class=\"breadcrumb-item active\">Advance Card
                                  </li> -->
                              </ol>
                          </div>
                      </div>
                  </div>
                  <div class=\"form-group form-group-compose text-center compose-btn\">
                      <button type=\"button\" class=\"btn btn-primary btn-block my-2\" data-toggle=\"modal\" data-target=\"#createForm\"><i class=\"feather icon-edit\"></i> Nuevo mensaje</button>
                  </div>
              </div>
              </div>
              </div>
        <div class=\"content-area-wrapper\">


            <div class=\"sidebar-left\">
                <div class=\"sidebar\">

                    <div class=\"sidebar-content email-app-sidebar d-flex\">
                        <span class=\"sidebar-close-icon\">
                            <i class=\"feather icon-x\"></i>
                        </span>
                        <div class=\"email-app-menu\">
                            <div class=\"form-group form-group-compose text-center compose-btn\">
                                <!-- <button type=\"button\" class=\"btn btn-primary btn-block my-2\" data-toggle=\"modal\" data-target=\"#composeForm\"><i class=\"feather icon-edit\"></i> Compose</button> -->
                            </div>
                            <div class=\"sidebar-menu-list\">
                                <div class=\"list-group list-group-messages font-medium-1\">
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0 pt-0 active\">
                                      <br>
                                        <i class=\"font-medium-5 feather icon-mail mr-50\"></i> Mensajes <span class=\"badge badge-primary badge-pill float-right\"></span>
                                    </a>
                                    <!-- <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 fa fa-paper-plane-o mr-50\"></i> Sent</a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-edit-2 mr-50\"></i> Draft <span class=\"badge badge-warning badge-pill float-right\">4</span> </a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-star mr-50\"></i>
                                        Starred</a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-info mr-50\"></i>
                                        Spam <span class=\"badge badge-danger badge-pill float-right\">3</span> </a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0\"><i class=\"font-medium-5 feather icon-trash mr-50\"></i>
                                        Trash</a> -->
                                </div>
                                <hr>
                                <h5 class=\"my-2 pt-25\">Tipos de mensajes</h5>
                                <div class=\"list-group list-group-labels font-medium-1\">
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0 d-flex align-items-center\"><span class=\"bullet bullet-danger mr-1\"></span> Consultas</a>
                                    <!-- <a href=\"#\" class=\"list-group-item list-group-item-action border-0 d-flex align-items-center\"><span class=\"bullet bullet-primary mr-1\"></span> CUrs</a>
                                    <a href=\"#\" class=\"list-group-item list-group-item-action border-0 d-flex align-items-center\"><span class=\"bullet bullet-warning mr-1\"></span> Important</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class=\"modal fade text-left\" id=\"composeForm\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"emailCompose\" aria-hidden=\"true\">
                        <div class=\"modal-dialog modal-dialog-scrollable\">
                            <form class=\"modal-content\" id=\"c-form\" method=\"POST\" enctype=\"multipart/form-data\">
                                <div class=\"modal-header\">
                                    <h3 class=\"modal-title text-text-bold-600\" id=\"emailCompose\">Respuesta</h3>
                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\">&times;</span>
                                    </button>
                                </div>
                                <div class=\"modal-body pt-1\">
                                    <div id=\"email-container\">
                                        <textarea name=\"contenido\" required minlength=\"10\" style=\"width: 470px;height: 150px;\"></textarea>
                                    </div>
                                    <div class=\"form-group mt-2\">
                                        <div class=\"custom-file\">
                                            <input type=\"file\" class=\"custom-file-input\" id=\"emailAttach\" name=\"file\">
                                            <label class=\"custom-file-label\" for=\"emailAttach\">Archivos</label>
                                        </div>
                                    </div>
                                </div>
                                <input type='hidden' name='alumnoId' value='{{app.request.get('alumnoId')}}' />
                                <div class=\"modal-footer\">
                                    <input type=\"submit\" value=\"Enviar\" class=\"btn btn-primary\">
                                    <input type=\"Reset\" value=\"Cancelar\" class=\"btn btn-white\" data-dismiss=\"modal\">
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class=\"modal fade text-left\" id=\"createForm\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"emailCompose\" aria-hidden=\"true\">
                        <div class=\"modal-dialog modal-dialog-scrollable\">
                            <form class=\"modal-content\" id=\"create-form\" action='/crear-mensaje' method=\"POST\" enctype=\"multipart/form-data\">
                                <div class=\"modal-header\">
                                    <h3 class=\"modal-title text-text-bold-600\" id=\"emailCompose\">Crear mensaje</h3>
                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\">&times;</span>
                                    </button>
                                </div>
                                <div class=\"modal-body pt-1\">
                                <div id=\"email-container\">
                                    <label class=\"label\" for=\"title\">Asunto</label>
                                        <input name=\"titulo\" id='title' required minlength=\"5\" style=\"width: 470px\"/>
                                    </div>
                                    <div id=\"email-container\">
                                        <label class=\"label\" for=\"content\">Contenido</label>
                                        <textarea name=\"contenido\" id='content' required minlength=\"10\" style=\"width: 470px;height: 150px;\"></textarea>
                                    </div>
                                    {% set bu = app.session.get('bu') %}
                                    {% if bu == '1' %}
                                        <div id=\"email-container\">
                                        <label class=\"label\" for=\"docente\">Docente</label>
                                        <select required class='form-control' name='docente'>
                                            {% for p in profesores %}
                                                <option value=\"{{p.id}}\">{{p.nombre}} {{p.apellido}}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                    {% endif %}
                                    
                                    
                                    <div class=\"form-group mt-2\">
                                        <div class=\"custom-file\">
                                            <input type=\"file\" class=\"custom-file-input\" id=\"emailAttach\" name=\"file\">
                                            <label class=\"custom-file-label\" for=\"emailAttach\">Archivos</label>
                                        </div>
                                    </div>

                                    
                                </div>
                                <input type='hidden' name='alumnoId' value='{{app.request.get('alumnoId')}}' />
                                <div class=\"modal-footer\">
                                    <input type=\"submit\" value=\"Enviar\" class=\"btn btn-primary\">
                                    <input type=\"Reset\" value=\"Cancelar\" class=\"btn btn-white\" data-dismiss=\"modal\">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class=\"content-right\">
                <div class=\"content-wrapper\">
                    <div class=\"content-header row\">
                    </div>
                    <div class=\"content-body\">
                        <div class=\"app-content-overlay\"></div>
                        <div class=\"email-app-area\">
                            <!-- Email list Area -->
                            <div class=\"email-app-list-wrapper\">
                                <div class=\"email-app-list\">
                                    <div class=\"app-fixed-search\">
                                        <div class=\"sidebar-toggle d-block d-lg-none\"><i class=\"feather icon-menu\"></i></div>
                                        <fieldset class=\"form-group position-relative has-icon-left m-0\">
                                            <input type=\"text\" class=\"form-control\" id=\"email-search\" placeholder=\"Buscar mensaje\">
                                            <div class=\"form-control-position\">
                                                <i class=\"feather icon-search\"></i>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class=\"email-user-list list-group\">
                                        <ul class=\"users-list-wrapper media-list\">
                                        {% set logoUrl = app.session.get('logoUrl') %}
                                        {% set user = app.session.get('user') %}
                                        
                                        {% for c in comunicados %}
                                            <li class=\"media mail{% if c.leido == true %}-read{% endif %}\" id=\"{{c.id}}\" onclick='fetch(\"/crear-lectura/{{c.id}}\");\$(\"#{{c.id}}\").addClass(\"mail-read\")'>
                                                <div class=\"media-left pr-50\">
                                                    <div class=\"avatar\">
                                                        <img src=\"{{logoUrl}}\" alt=\"avtar img holder\">
                                                    </div>

                                                </div>
                                                <div class=\"media-body\">
                                                    <div class=\"user-details\">
                                                        <div class=\"mail-items\">
                                                            <h5 class=\"list-group-item-heading text-bold-600 mb-25\">{{c.titulo}}</h5>
                                                            <span class=\"list-group-item-text text-truncate\">{{c.tipo}}</span>
                                                        </div>
                                                        <div class=\"mail-meta-item\">
                                                            <span class=\"float-right\">
                                                                <span class=\"mr-1 bullet {{c.tipo == 'informativo' ? 'bullet-success' : 'bullet-warning'}} bullet-sm\"></span><span class=\"mail-date\">{{c.fechaCreacion|date('h:i a')}}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class=\"mail-message\">
                                                        <p class=\"list-group-item-text truncate mb-0\">{{c.contenido}}.</p>
                                                    </div>
                                                </div>
                                            </li>
                                        {% endfor %}
                                        </ul>
                                        <div class=\"no-results\">
                                            <h5>No hay mensajes</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ Email list Area -->
                            <!-- Detailed Email View -->
                            {% for c in comunicados %}
                            <div class=\"email-app-details\" id=\"detalle-{{c.id}}\">
                                <div class=\"email-detail-header\">
                                    <div class=\"email-header-left d-flex align-items-center mb-1\">
                                        <span class=\"go-back mr-1\"><i class=\"feather icon-arrow-left font-medium-4\"></i></span>
                                        <h3>{{c.titulo}}</h3>
                                    </div>
                                    <div class=\"email-header-right mb-1 ml-2 pl-1\">
                                        <ul class=\"list-inline m-0\">
                                            <!-- <li class=\"list-inline-item\"><span class=\"action-icon favorite\"><i class=\"feather icon-star font-medium-5\"></i></span></li> -->
                                            <li class=\"list-inline-item\">
                                                <div class=\"dropdown no-arrow\">
                                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                        <!-- <i class=\"feather icon-folder font-medium-5\"></i> -->
                                                    </a>
                                                    <!-- <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"folder\">
                                                        <a class=\"dropdown-item d-flex font-medium-1\" href=\"#\"><i class=\"font-medium-3 feather icon-edit-2 mr-50\"></i> Draft</a>
                                                        <a class=\"dropdown-item d-flex font-medium-1\" href=\"#\"><i class=\"font-medium-3 feather icon-info mr-50\"></i> Spam</a>
                                                        <a class=\"dropdown-item d-flex font-medium-1\" href=\"#\"><i class=\"font-medium-3 feather icon-trash mr-50\"></i> Trash</a>
                                                    </div> -->
                                                </div>
                                            </li>
                                            <li class=\"list-inline-item\">

                                            </li>
                                            <!-- <li class=\"list-inline-item\"><span class=\"action-icon\"><i class=\"feather icon-mail font-medium-5\"></i></span></li>
                                            <li class=\"list-inline-item\"><span class=\"action-icon\"><i class=\"feather icon-trash font-medium-5\"></i></span></li> -->
                                            {# <li class=\"list-inline-item email-prev\"><span class=\"action-icon\"><i class=\"feather icon-chevrons-left font-medium-5\"></i></span></li>
                                            <li class=\"list-inline-item email-next\"><span class=\"action-icon\"><i class=\"feather icon-chevrons-right font-medium-5\"></i></span></li> #}
                                        </ul>
                                    </div>
                                </div>
                                <div class=\"email-scroll-area\" style=\"overflow-y: auto;\">
                                    <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"email-label ml-2 my-2 pl-1\">
                                                <span class=\"mr-1 bullet {{c.tipo == 'informativo' ? 'bullet-success' : 'bullet-danger' }} bullet-sm\"></span><small class=\"mail-label\">{{c.tipo}}</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"card px-1\">
                                                <div class=\"card-header email-detail-head ml-75\">
                                                    <div class=\"user-details d-flex justify-content-between align-items-center flex-wrap\">
                                                        <div class=\"avatar mr-75\">
                                                            <img src=\"{{logoUrl}}\" alt=\"avtar img holder\" width=\"61\" height=\"61\">
                                                        </div>
                                                        <div class=\"mail-items\">
                                                            <h4 class=\"list-group-item-heading mb-0\">
                                                                {% if 'ROLE_PRECEPTOR' in c.creador.roles %}
                                                                    Preceptoria
                                                                {% endif %}
                                                                {% if 'ROLE_DIRECTOR' in c.creador.roles %}
                                                                    Dirección
                                                                {% endif %}
                                                                {% if 'ROLE_MAESTRO' in c.creador.roles %}
                                                                    Profesor
                                                                {% endif %}
                                                            </h4>
                                                            <div class=\"email-info-dropup dropdown\">
                                                                <span class=\"font-small-3\" id=\"dropdownMenuButton200\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                                    {{c.creador.nombre}} {{c.creador.apellido}}
                                                                </span>
                                                                <!-- <div class=\"dropdown-menu dropdown-menu-right p-50\" aria-labelledby=\"dropdownMenuButton200\">
                                                                    <div class=\"px-25 dropdown-item\">From: <strong> abaldersong@utexas.edu </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">To: <strong> johndoe@ow.ly </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">Date: <strong> 4:25 AM 13 Jan 2018 </strong></div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"mail-meta-item\">
                                                        <div class=\"mail-time mb-1\">{{c.fechaCreacion|date('h:i a')}}</div>
                                                        <div class=\"mail-date\">{{c.fechaCreacion|date('d/m/Y')}}</div>
                                                    </div>
                                                </div>
                                                <div class=\"card-body mail-message-wrapper pt-2 mb-0\">
                                                    <div class=\"mail-message\" style=\"white-space: pre;\">{{c.contenido}}</div>
                                                    <div class=\"mail-attachements d-flex\">
                                                        <i class=\"feather icon-paperclip font-medium-5 mr-50\"></i>
                                                        <span>Documentos</span>
                                                    </div>
                                                </div>
                                                <div class=\"mail-files py-2\">
                                                {% for a in c.archivos %}
                                                    <div class=\"chip chip-primary\">
                                                        <div class=\"chip-body py-50\">
                                                        
                                                            <a class=\"chip-text\" style=\"color: #FFF;\" href=\"/images/{{a.url}}\" target=\"_blank\">{{a.nombre}}</a>
                                                            
                                                        </div>
                                                    </div>
                                                    {% endfor %}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {% for c in c.respuestas %}
                                        {% if c.aprobado == 1 or (c.aprobado != 1 and c.creador.id == user.id)%}
                                        <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"card px-1\">
                                                <div class=\"card-header email-detail-head ml-75\">
                                                    <div class=\"user-details d-flex justify-content-between align-items-center flex-wrap\">
                                                        <div class=\"avatar mr-75\">
                                                            <img src=\"{{logoUrl}}\" alt=\"avtar img holder\" width=\"61\" height=\"61\">
                                                        </div>
                                                        <div class=\"mail-items\">
                                                            <h4 class=\"list-group-item-heading mb-0\">
                                                                RESPUESTA
                                                            </h4>
                                                            <div class=\"email-info-dropup dropdown\">
                                                                <span class=\"font-small-3\" id=\"dropdownMenuButton200\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                                    {{c.creador.nombre}} {{c.creador.apellido}}
                                                                </span>
                                                                <!-- <div class=\"dropdown-menu dropdown-menu-right p-50\" aria-labelledby=\"dropdownMenuButton200\">
                                                                    <div class=\"px-25 dropdown-item\">From: <strong> abaldersong@utexas.edu </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">To: <strong> johndoe@ow.ly </strong></div>
                                                                    <div class=\"px-25 dropdown-item\">Date: <strong> 4:25 AM 13 Jan 2018 </strong></div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=\"mail-meta-item\">
                                                        <div class=\"mail-time mb-1\">{{c.fechaCreacion|date('h:i a')}}</div>
                                                        <div class=\"mail-date\">{{c.fechaCreacion|date('d/m/Y')}}</div>
                                                    </div>
                                                </div>
                                                <div class=\"card-body mail-message-wrapper pt-2 mb-0\">
                                                    <div class=\"mail-message\" style=\"white-space: pre;\">{{c.contenido}}</div>
                                                    <div class=\"mail-attachements d-flex\">
                                                        <i class=\"feather icon-paperclip font-medium-5 mr-50\"></i>
                                                        <span>Documentos</span>
                                                    </div>
                                                </div>
                                                <div class=\"mail-files py-2\">
                                                {% for a in c.archivos %}
                                                    <div class=\"chip chip-primary\">
                                                        <div class=\"chip-body py-50\">
                                                        
                                                            <a class=\"chip-text\" style=\"color: #FFF;\" href=\"/images/{{a.url}}\" target=\"_blank\">{{a.nombre}}</a>
                                                            
                                                        </div>
                                                    </div>
                                                    {% endfor %}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {% endif %}
                                    {% endfor %}



                                        
                                    {% if c.tipo == 'consulta' %}
                                    <div class=\"row\">
                                        <div class=\"col-12\">
                                            <div class=\"card\">


                                                <div class=\"card-body\">
                                                    <div class=\"d-flex justify-content-between\">

                                                        <!-- <span class=\"font-medium-1\">El comunicado es de caracter informativo <span class=\"primary cursor-pointer\"><strong>No se puede responder</strong></span> </span>
                                                        <i class=\"feather icon-paperclip font-medium-5 mr-50\"></i> -->
                                                    </div>
                                                    <div class=\"form-group form-group-compose text-center compose-btn\">
                                                        <button type=\"button\" class=\"btn btn-primary btn-block my-2\" data-toggle=\"modal\" data-target=\"#composeForm\"><i class=\"feather icon-edit\"></i> Responder</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {% endif %}
                                </div>
                            </div>
                            {% endfor %}
                            <!--/ Detailed Email View -->
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END: Content-->

    <div class=\"sidenav-overlay\"></div>
    <div class=\"drag-target\"></div>

    <!-- BEGIN: Footer-->
    <footer class=\"footer footer-static footer-light\">
        <p class=\"clearfix blue-grey lighten-2 mb-0\"><span class=\"float-md-left d-block d-md-inline-block mt-25\">Acricana - Cuaderno de Comunicados V0.1 - Powered by <a class=\"text-bold-800 grey darken-2\" href=\"#\" target=\"_blank\">Movius</a></span><span class=\"float-md-right d-none d-md-block\">Hand-crafted & Made with<i class=\"feather icon-heart pink\"></i></span>
            <button class=\"btn btn-primary btn-icon scroll-top\" type=\"button\"><i class=\"feather icon-arrow-up\"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/editors/quill/katex.min.js\"></script>
    <script src=\"../../../app-assets/vendors/js/editors/quill/highlight.min.js\"></script>
    <script src=\"../../../app-assets/vendors/js/editors/quill/quill.min.js\"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->
    <script>
    if (\"serviceWorker\" in navigator) {
  window.addEventListener(\"load\", function() {
    navigator.serviceWorker
      .register(\"{{ asset('sw.js') }}\")
      .then(res => console.log(\"service worker registered\"))
      .catch(err => console.log(\"service worker not registered\", err))
      })
    }
    </script>
    <!-- BEGIN: Page JS-->
    <script src=\"../../../app-assets/js/scripts/pages/app-email2.js\"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
", "mensajes.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/mensajes.html.twig");
    }
}
