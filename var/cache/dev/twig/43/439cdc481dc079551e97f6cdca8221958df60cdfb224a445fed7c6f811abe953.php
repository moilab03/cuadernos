<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* auth-login.html.twig */
class __TwigTemplate_7cc8585148718bbc8806f58a075c0d6a6cf0b52c12e004a311883270abda2596 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "auth-login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "auth-login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Login</title>
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/dark-layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/authentication.css\">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../assets/css/style.css\">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern  1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page\" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"1-column\" data-layout=\"dark-layout\">
    <!-- BEGIN: Content-->
    <div class=\"app-content content\">
        <div class=\"content-overlay\"></div>
        <div class=\"header-navbar-shadow\"></div>
        <div class=\"content-wrapper\">
            <div class=\"content-header row\">
            </div>
            ";
        // line 52
        if (((isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 52, $this->source); })()) != "")) {
            // line 53
            echo "                <div class=\"alert alert-warning\" role=\"alert\">
                    ";
            // line 54
            echo twig_escape_filter($this->env, (isset($context["message"]) || array_key_exists("message", $context) ? $context["message"] : (function () { throw new RuntimeError('Variable "message" does not exist.', 54, $this->source); })()), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 57
        echo "            ";
        if (((isset($context["message2"]) || array_key_exists("message2", $context) ? $context["message2"] : (function () { throw new RuntimeError('Variable "message2" does not exist.', 57, $this->source); })()) != "")) {
            // line 58
            echo "                <div class=\"alert alert-success\" role=\"alert\">
                    ";
            // line 59
            echo twig_escape_filter($this->env, (isset($context["message2"]) || array_key_exists("message2", $context) ? $context["message2"] : (function () { throw new RuntimeError('Variable "message2" does not exist.', 59, $this->source); })()), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 62
        echo "            <div class=\"content-body\">
                <section class=\"row flexbox-container\">
                    <div class=\"col-xl-8 col-11 d-flex justify-content-center\">
                        <div class=\"card bg-authentication rounded-0 mb-0\">
                            <div class=\"row m-0\">
                                <div class=\"col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0\">
                                    <img src=\"../../../app-assets/images/pages/login.png\" alt=\"branding logo\">
                                </div>
                                <div class=\"col-lg-6 col-12 p-0\">
                                    <div class=\"card rounded-0 mb-0 px-2\">
                                        <div class=\"card-header pb-1\">
                                            <div class=\"card-title\">
                                                <h4 class=\"mb-0\">Acceso</h4>
                                            </div>
                                        </div>
                                        <p class=\"px-2\">Bienvenido! Ingrese a su cuenta.</p>
                                        <div class=\"card-content\">
                                            <div class=\"card-body pt-1\">
                                                <form action=\"/\" method=\"POST\">
                                                  <!-- <fieldset class=\"form-group\">
                                                      <select class=\"form-control\" id=\"basicSelect\">
                                                          <option>Seleccione</option>
                                                          <option>Acricana</option>
                                                          <option>ALS</option>
                                                      </select>
                                                  </fieldset> -->
                                                    <fieldset class=\"form-label-group form-group position-relative has-icon-left\">
                                                        <input type=\"email\" class=\"form-control\" id=\"user-name\" placeholder=\"email\" required name=\"username\">
                                                        <div class=\"form-control-position\">
                                                            <i class=\"feather icon-user\"></i>
                                                        </div>
                                                        <label for=\"user-name\">Email</label>
                                                    </fieldset>

                                                    <fieldset class=\"form-label-group position-relative has-icon-left\">
                                                        <input type=\"password\" class=\"form-control\" id=\"user-password\" placeholder=\"******\" required name=\"password\">
                                                        <div class=\"form-control-position\">
                                                            <i class=\"feather icon-lock\"></i>
                                                        </div>
                                                        <label for=\"user-password\">Password</label>
                                                    </fieldset>
                                                    <div class=\"form-group d-flex justify-content-between align-items-center\">
                                                        <div class=\"text-left\">
                                                            <fieldset class=\"checkbox\">
                                                                <div class=\"vs-checkbox-con vs-checkbox-primary\">
                                                                    <input type=\"checkbox\">
                                                                    <span class=\"vs-checkbox\">
                                                                        <span class=\"vs-checkbox--check\">
                                                                            <i class=\"vs-icon feather icon-check\"></i>
                                                                        </span>
                                                                    </span>
                                                                    <span class=\"\">Recordarme</span>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class=\"text-right\"><a href=\"/recuperar-contrasena\" class=\"card-link\">olvide mi contrasena</a></div>
                                                    </div>

                                                    <button type=\"submit\" class=\"btn btn-primary float-right btn-inline\">Entrar</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class=\"login-footer\">
                                            <div class=\"divider\">

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "auth-login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 62,  113 => 59,  110 => 58,  107 => 57,  101 => 54,  98 => 53,  96 => 52,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Login</title>
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/dark-layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/authentication.css\">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../assets/css/style.css\">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern  1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page\" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"1-column\" data-layout=\"dark-layout\">
    <!-- BEGIN: Content-->
    <div class=\"app-content content\">
        <div class=\"content-overlay\"></div>
        <div class=\"header-navbar-shadow\"></div>
        <div class=\"content-wrapper\">
            <div class=\"content-header row\">
            </div>
            {% if message != '' %}
                <div class=\"alert alert-warning\" role=\"alert\">
                    {{message}}
                </div>
            {% endif %}
            {% if message2 != '' %}
                <div class=\"alert alert-success\" role=\"alert\">
                    {{message2}}
                </div>
            {% endif %}
            <div class=\"content-body\">
                <section class=\"row flexbox-container\">
                    <div class=\"col-xl-8 col-11 d-flex justify-content-center\">
                        <div class=\"card bg-authentication rounded-0 mb-0\">
                            <div class=\"row m-0\">
                                <div class=\"col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0\">
                                    <img src=\"../../../app-assets/images/pages/login.png\" alt=\"branding logo\">
                                </div>
                                <div class=\"col-lg-6 col-12 p-0\">
                                    <div class=\"card rounded-0 mb-0 px-2\">
                                        <div class=\"card-header pb-1\">
                                            <div class=\"card-title\">
                                                <h4 class=\"mb-0\">Acceso</h4>
                                            </div>
                                        </div>
                                        <p class=\"px-2\">Bienvenido! Ingrese a su cuenta.</p>
                                        <div class=\"card-content\">
                                            <div class=\"card-body pt-1\">
                                                <form action=\"/\" method=\"POST\">
                                                  <!-- <fieldset class=\"form-group\">
                                                      <select class=\"form-control\" id=\"basicSelect\">
                                                          <option>Seleccione</option>
                                                          <option>Acricana</option>
                                                          <option>ALS</option>
                                                      </select>
                                                  </fieldset> -->
                                                    <fieldset class=\"form-label-group form-group position-relative has-icon-left\">
                                                        <input type=\"email\" class=\"form-control\" id=\"user-name\" placeholder=\"email\" required name=\"username\">
                                                        <div class=\"form-control-position\">
                                                            <i class=\"feather icon-user\"></i>
                                                        </div>
                                                        <label for=\"user-name\">Email</label>
                                                    </fieldset>

                                                    <fieldset class=\"form-label-group position-relative has-icon-left\">
                                                        <input type=\"password\" class=\"form-control\" id=\"user-password\" placeholder=\"******\" required name=\"password\">
                                                        <div class=\"form-control-position\">
                                                            <i class=\"feather icon-lock\"></i>
                                                        </div>
                                                        <label for=\"user-password\">Password</label>
                                                    </fieldset>
                                                    <div class=\"form-group d-flex justify-content-between align-items-center\">
                                                        <div class=\"text-left\">
                                                            <fieldset class=\"checkbox\">
                                                                <div class=\"vs-checkbox-con vs-checkbox-primary\">
                                                                    <input type=\"checkbox\">
                                                                    <span class=\"vs-checkbox\">
                                                                        <span class=\"vs-checkbox--check\">
                                                                            <i class=\"vs-icon feather icon-check\"></i>
                                                                        </span>
                                                                    </span>
                                                                    <span class=\"\">Recordarme</span>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class=\"text-right\"><a href=\"/recuperar-contrasena\" class=\"card-link\">olvide mi contrasena</a></div>
                                                    </div>

                                                    <button type=\"submit\" class=\"btn btn-primary float-right btn-inline\">Entrar</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class=\"login-footer\">
                                            <div class=\"divider\">

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
", "auth-login.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/auth-login.html.twig");
    }
}
