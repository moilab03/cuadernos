<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* fields/aprobado.html.twig */
class __TwigTemplate_9dcf20c7e93db3d8a866742befa9c6327c04b5c654010dd9d5ec76de129ef1c2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "fields/aprobado.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "fields/aprobado.html.twig"));

        // line 1
        if ((twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 1, $this->source); })()), "aprobado", [], "any", false, false, false, 1) == 0)) {
            // line 2
            echo "<div style='background-color: grey; width: 100px; heigth: 25px; border-radius: 15px; color: #FFF; text-align: center;'>
    Pendiente
</div>
";
        }
        // line 6
        if ((twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 6, $this->source); })()), "aprobado", [], "any", false, false, false, 6) == 1)) {
            // line 7
            echo "<div style='background-color: green; width: 100px; heigth: 25px; border-radius: 15px; color: #FFF; text-align: center;'>
    Aprobado
</div>
";
        }
        // line 11
        if ((twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 11, $this->source); })()), "aprobado", [], "any", false, false, false, 11) == 2)) {
            // line 12
            echo "<div style='background-color: red; width: 100px; heigth: 25px; border-radius: 15px; color: #FFF; text-align: center;'>
    Rechazado
</div>
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "fields/aprobado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 12,  59 => 11,  53 => 7,  51 => 6,  45 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if item.aprobado == 0 %}
<div style='background-color: grey; width: 100px; heigth: 25px; border-radius: 15px; color: #FFF; text-align: center;'>
    Pendiente
</div>
{% endif %}
{% if item.aprobado == 1 %}
<div style='background-color: green; width: 100px; heigth: 25px; border-radius: 15px; color: #FFF; text-align: center;'>
    Aprobado
</div>
{% endif %}
{% if item.aprobado == 2 %}
<div style='background-color: red; width: 100px; heigth: 25px; border-radius: 15px; color: #FFF; text-align: center;'>
    Rechazado
</div>
{% endif %}", "fields/aprobado.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/fields/aprobado.html.twig");
    }
}
