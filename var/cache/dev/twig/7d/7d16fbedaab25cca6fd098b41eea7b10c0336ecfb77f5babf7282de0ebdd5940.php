<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* alumno.html.twig */
class __TwigTemplate_a7e11d7cdeae5a1c6c2a3e700df9f49bfd6a2c66adee65f40061c80781e098e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "alumno.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "alumno.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Acricana</title>
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/dashboard-ecommerce.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/users.css\">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../assets/css/style.css\">
    <!-- END: Custom CSS-->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js\"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js\"></script>

  <!-- Add Firebase products that you want to use -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js\"></script>
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js\"></script>
  ";
        // line 47
        $context["user"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 47, $this->source); })()), "session", [], "any", false, false, false, 47), "get", [0 => "user"], "method", false, false, false, 47);
        // line 48
        echo "  <script> 
  var userId = ";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 49, $this->source); })()), "id", [], "any", false, false, false, 49), "html", null, true);
        echo "; 
  </script>
 <script src=\"/firebase-messaging-sw.js\"></script>

 <!-- JavaScript -->
<script src=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js\"></script>

<!-- CSS -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css\"/>
<!-- Default theme -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css\"/>



</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern  2-columns  navbar-floating footer-static  \" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"2-columns\" data-layout=\"dark-layout\">

    ";
        // line 70
        $this->loadTemplate("menu.html.twig", "alumno.html.twig", 70)->display($context);
        // line 71
        echo "
    <!-- BEGIN: Content-->
    <div class=\"app-content content\">
        <div class=\"content-overlay\"></div>
        <div class=\"header-navbar-shadow\"></div>
        <div class=\"content-wrapper\">
            <div class=\"content-header row\">
                <div class=\"content-header-left col-md-9 col-12 mb-2\">
                    <div class=\"row breadcrumbs-top\">
                        <div class=\"col-12\">
                            <h2 class=\"content-header-title float-left mb-0\">Dashboard</h2>
                            <div class=\"breadcrumb-wrapper col-12\">
                                <ol class=\"breadcrumb\">
                                    <li class=\"breadcrumb-item\"><a href=\"/dashboard\">";
        // line 84
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 84, $this->source); })()), "nombre", [], "any", false, false, false, 84), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 84, $this->source); })()), "apellido", [], "any", false, false, false, 84), "html", null, true);
        echo "</a>
                                    </li>
                                    <!-- <li class=\"breadcrumb-item\"><a href=\"#\">Card</a>
                                    </li>
                                    <li class=\"breadcrumb-item active\">Advance Card
                                    </li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"content-body\">
              <div id=\"user-profile\">
                  <div class=\"row\">
                      <div class=\"col-12\">
                          <div class=\"profile-header mb-2\">
                              <div class=\"relative\">
                                  <div class=\"cover-container\">
                                      <img class=\"img-fluid bg-cover rounded-0 w-100\" src=\"../../../app-assets/images/profile/user-uploads/cover.png\" alt=\"User Profile Image\">
                                  </div>
                                  <div class=\"profile-img-container d-flex align-items-center justify-content-between\">
                                      <img src=\"";
        // line 107
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 107, $this->source); })()), "businessUnit", [], "any", false, false, false, 107), "logoUrl", [], "any", false, false, false, 107), "html", null, true);
        echo "\" class=\"rounded-circle img-border box-shadow-1\" alt=\"Card image\">
                                      <!-- <div class=\"float-right\">
                                          <button type=\"button\" class=\"btn btn-icon btn-icon rounded-circle btn-primary mr-1\">
                                              <i class=\"feather icon-edit-2\"></i>
                                          </button>
                                          <button type=\"button\" class=\"btn btn-icon btn-icon rounded-circle btn-primary\">
                                              <i class=\"feather icon-settings\"></i>
                                          </button>
                                      </div> -->
                                  </div>
                              </div>
                              <div class=\"d-flex justify-content-end align-items-center profile-header-nav\">
                                  <!-- <nav class=\"navbar navbar-expand-sm w-100 pr-0\">
                                      <button class=\"navbar-toggler pr-0\" type=\"button\" data-toggle=\"collapse\" data-target=\"navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                          <span class=\"navbar-toggler-icon\"><i class=\"feather icon-align-justify\"></i></span>
                                      </button>
                                      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                          <ul class=\"navbar-nav justify-content-around w-75 ml-sm-auto\">
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Timeline</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">About</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Photos</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Friends</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Videos</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Events</a>
                                              </li>
                                          </ul>
                                      </div>
                                  </nav> -->
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
              <!-- Timeline Starts -->
              <section id=\"timeline-card\">
                  <div class=\"row\">
                      <div class=\"col-lg-6 col-sm-12\">
                        <div class=\"card\">
                            <div class=\"card-header\">
                                <h4>";
        // line 157
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 157, $this->source); })()), "nombre", [], "any", false, false, false, 157), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 157, $this->source); })()), "apellido", [], "any", false, false, false, 157), "html", null, true);
        echo "</h4><br>
                            </div>
                            <div class=\"card-body\">
                                <!-- <p>Tart I love sugar plum I love oat cake. Sweet roll caramels I love jujubes. Topping cake wafer.</p> -->
                                <div class=\"mt-1\">
                                    <p><strong>Ciclo: </strong>";
        // line 162
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 162, $this->source); })()), "cursos", [], "any", false, false, false, 162), "ciclo", [], "any", false, false, false, 162), "nombre", [], "any", false, false, false, 162), "html", null, true);
        echo " <strong>Curso: </strong> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 162, $this->source); })()), "cursos", [], "any", false, false, false, 162), "nombre", [], "any", false, false, false, 162), "html", null, true);
        echo " <strong>Division: </strong> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 162, $this->source); })()), "cursos", [], "any", false, false, false, 162), "division", [], "any", false, false, false, 162), "nombre", [], "any", false, false, false, 162), "html", null, true);
        echo "  </p>
                                </div>
                                <div class=\"mt-1\">
                                  <h6 class=\"mb-0\">Preceptores:</h6>
                                  ";
        // line 166
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 166, $this->source); })()), "cursos", [], "any", false, false, false, 166), "preceptores", [], "any", false, false, false, 166));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 167
            echo "                                  <p>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nombre", [], "any", false, false, false, 167), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "apellido", [], "any", false, false, false, 167), "html", null, true);
            echo "</p>
                                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 169
        echo "                                </div>
                                <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">DNI / Pasaporte:</h6>
                                    <p>";
        // line 172
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 172, $this->source); })()), "dni", [], "any", false, false, false, 172), "html", null, true);
        echo "</p>
                                </div>
                                <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">Padres:</h6>
                                    ";
        // line 176
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 176, $this->source); })()), "padres", [], "any", false, false, false, 176));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 177
            echo "                                        <p>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "nombre", [], "any", false, false, false, 177), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "apellido", [], "any", false, false, false, 177), "html", null, true);
            echo ": <storng>Cel</strong> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "telefono", [], "any", false, false, false, 177), "html", null, true);
            echo "
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 179
        echo "                                    
                                </div>
                                <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">Direccion:</h6>
                                    <p>";
        // line 183
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 183, $this->source); })()), "direccion", [], "any", false, false, false, 183), "html", null, true);
        echo "</p>

                                </div>

                                <!-- <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">Website:</h6>
                                    <p>www.pixinvent.com</p>
                                </div> -->
                                <!-- <div class=\"mt-1\">
                                    <button type=\"button\" class=\"btn btn-sm btn-icon btn-primary mr-25 p-25\"><i class=\"feather icon-facebook\"></i></button>
                                    <button type=\"button\" class=\"btn btn-sm btn-icon btn-primary mr-25 p-25\"><i class=\"feather icon-twitter\"></i></button>
                                    <button type=\"button\" class=\"btn btn-sm btn-icon btn-primary p-25\"><i class=\"feather icon-instagram\"></i></button>
                                </div> -->
                            </div>
                        </div>
                      </div>
                      <div class=\"col-lg-6 col-sm-12\">
                        <div class=\"card\">
                            <div class=\"card-header d-flex justify-content-between\">
                                <h4>Información</h4>
                                <i class=\"feather icon-more-horizontal cursor-pointer\"></i>
                            </div>

                            <div class=\"card-body\">
                            <div class=\"d-flex justify-content-start align-items-center mb-1\">

                                  <div class=\"user-page-info\">
                                      <h6 class=\"mb-0\">Información de la cursada</h6>
                                      <span class=\"font-small-2\">Materias, Horarios, Plan de estudios</span>
                                  </div>
                                  ";
        // line 213
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 213, $this->source); })()), "cursos", [], "any", false, false, false, 213), "horario", [], "any", false, false, false, 213) != null)) {
            // line 214
            echo "                                    <a href='/images/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 214, $this->source); })()), "cursos", [], "any", false, false, false, 214), "horario", [], "any", false, false, false, 214), "html", null, true);
            echo "' class=\"btn btn-primary btn-icon ml-auto\"><i class=\"feather icon-file\"></i></a>
                                    ";
        }
        // line 216
        echo "                            </div>
                              <h4>Materias</h4>
                              ";
        // line 218
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 218, $this->source); })()), "cursos", [], "any", false, false, false, 218), "materias", [], "any", false, false, false, 218));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 219
            echo "                                <div class=\"d-flex justify-content-start align-items-center mb-1\">

                                    <div class=\"user-page-info\">
                                        <h6 class=\"mb-0\">";
            // line 222
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["m"], "nombre", [], "any", false, false, false, 222), "html", null, true);
            echo "</h6>
                                        <span class=\"font-small-2\">";
            // line 223
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "profesor", [], "any", false, false, false, 223), "nombre", [], "any", false, false, false, 223), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["m"], "profesor", [], "any", false, false, false, 223), "apellido", [], "any", false, false, false, 223), "html", null, true);
            echo "</span>
                                    </div>
                                      <!-- <button type=\"button\" class=\"btn btn-primary btn-icon ml-auto\"><i class=\"feather icon-file\"></i></button> -->
                                </div>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 228
        echo "                            </div>
                        </div>
                        <!-- <div class=\"card\">

                            <div class=\"card-content\">
                              <div class=\"card-body text-center\">
                                  <h4>Cuaderno de comunicados</h4>
                                  <p class=\"\">Cuarto Grado</p>
                                  <div class=\"card-btns d-flex justify-content-center\">
                                      <button class=\"btn btn-outline-primary\">ver mas</button>
                                  </div>
                                  <div class=\"card-btns d-flex justify-content-center\">
                                      <button class=\"btn btn-outline-primary\">ver mas</button>
                                  </div>
                                  <div class=\"card-btns d-flex justify-content-center\">
                                      <button class=\"btn btn-outline-primary\">ver mas</button>
                                  </div>

                              </div>
                            </div>
                        </div> -->
                  </div>
              </section>
              <!-- Timeline Ends -->
              <div class=\"card-header\">
                  <h4>Cuaderno de comunicados</h4>

              </div>
              <!-- Basic example and Profile cards section start -->
              <section id=\"basic-examples\">
                  <div class=\"row match-height\">
                      <div class=\"col-xl-6 col-md-6 col-sm-12\">
                        <div class=\"card\">
                            <div class=\"card-content\">
                                <div class=\"card-body\">
                                    <!-- <img class=\"card-img img-fluid mb-1\" src=\"../../../app-assets/images/pages/content-img-3.jpg\" alt=\"Card image cap\"> -->
                                    <h5 class=\"my-1\">Comunicados</h5>
                                    <!-- <div class=\"d-flex justify-content-between\">
                                        <small class=\"float-left font-weight-bold mb-25\" id=\"example-caption-1\">\$ 5975</small>
                                        <small class=\"float-right  mb-25\" id=\"example-caption-2\">\$ 8000</small>
                                    </div> -->
                                    <div class=\"progress progress-bar-primary box-shadow-6\">
                                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"75\" aria-valuemax=\"100\" style=\"width:100%\" aria-describedby=\"example-caption-2\"></div>
                                    </div>
                                    <div class=\"card-btns d-flex justify-content-between\">
                                        <!-- <a href=\"#\" class=\"btn gradient-light-primary white\">Download</a> -->
                                        <a href=\"/comunicados?alumnoId=";
        // line 274
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 274, $this->source); })()), "id", [], "any", false, false, false, 274), "html", null, true);
        echo "\" class=\"btn btn-outline-primary float-right\">Ver comunicados</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class=\"col-xl-6 col-md-6 col-sm-12\">
                          <div class=\"card\">
                              <div class=\"card-content\">
                                  <div class=\"card-body\">
                                      <!-- <img class=\"card-img img-fluid mb-1\" src=\"../../../app-assets/images/pages/content-img-3.jpg\" alt=\"Card image cap\"> -->
                                      <h5 class=\"my-1\">Mensajes</h5>
                                      <!-- <div class=\"d-flex justify-content-between\">
                                          <small class=\"float-left font-weight-bold mb-25\" id=\"example-caption-1\">\$ 5975</small>
                                          <small class=\"float-right  mb-25\" id=\"example-caption-2\">\$ 8000</small>
                                      </div> -->
                                      <div class=\"progress progress-bar-primary box-shadow-6\">
                                          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"75\" aria-valuemax=\"100\" style=\"width:100%\" aria-describedby=\"example-caption-2\"></div>
                                      </div>
                                      <div class=\"card-btns d-flex justify-content-between\">
                                          <!-- <a href=\"#\" class=\"btn gradient-light-primary white\">Download</a> -->
                                          <a href=\"/mensajes?alumnoId=";
        // line 296
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["alumno"]) || array_key_exists("alumno", $context) ? $context["alumno"] : (function () { throw new RuntimeError('Variable "alumno" does not exist.', 296, $this->source); })()), "id", [], "any", false, false, false, 296), "html", null, true);
        echo "\" class=\"btn btn-outline-primary float-right\">Ver mensajes</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Profile Cards Starts -->






            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class=\"sidenav-overlay\"></div>
    <div class=\"drag-target\"></div>

    <!-- BEGIN: Footer-->
    <footer class=\"footer footer-static footer-light\">
        <p class=\"clearfix blue-grey lighten-2 mb-0\"><span class=\"float-md-left d-block d-md-inline-block mt-25\">Acricana - Cuaderno de Comunicados V0.1 - Powered by <a class=\"text-bold-800 grey darken-2\" href=\"#\" target=\"_blank\">Movius</a></span><span class=\"float-md-right d-none d-md-block\">Hand-crafted & Made with<i class=\"feather icon-heart pink\"></i></span>
            <button class=\"btn btn-primary btn-icon scroll-top\" type=\"button\"><i class=\"feather icon-arrow-up\"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src=\"../../../app-assets/js/scripts/pages/app-chat.js\"></script>
        <script src=\"../../../app-assets/js/scripts/pages/user-profile.js\"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "alumno.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  433 => 296,  408 => 274,  360 => 228,  347 => 223,  343 => 222,  338 => 219,  334 => 218,  330 => 216,  324 => 214,  322 => 213,  289 => 183,  283 => 179,  270 => 177,  266 => 176,  259 => 172,  254 => 169,  243 => 167,  239 => 166,  228 => 162,  218 => 157,  165 => 107,  137 => 84,  122 => 71,  120 => 70,  96 => 49,  93 => 48,  91 => 47,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Acricana</title>
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/dashboard-ecommerce.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/users.css\">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../assets/css/style.css\">
    <!-- END: Custom CSS-->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js\"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js\"></script>

  <!-- Add Firebase products that you want to use -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js\"></script>
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js\"></script>
  {% set user = app.session.get('user') %}
  <script> 
  var userId = {{user.id}}; 
  </script>
 <script src=\"/firebase-messaging-sw.js\"></script>

 <!-- JavaScript -->
<script src=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js\"></script>

<!-- CSS -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css\"/>
<!-- Default theme -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css\"/>



</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern  2-columns  navbar-floating footer-static  \" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"2-columns\" data-layout=\"dark-layout\">

    {% include 'menu.html.twig' %}

    <!-- BEGIN: Content-->
    <div class=\"app-content content\">
        <div class=\"content-overlay\"></div>
        <div class=\"header-navbar-shadow\"></div>
        <div class=\"content-wrapper\">
            <div class=\"content-header row\">
                <div class=\"content-header-left col-md-9 col-12 mb-2\">
                    <div class=\"row breadcrumbs-top\">
                        <div class=\"col-12\">
                            <h2 class=\"content-header-title float-left mb-0\">Dashboard</h2>
                            <div class=\"breadcrumb-wrapper col-12\">
                                <ol class=\"breadcrumb\">
                                    <li class=\"breadcrumb-item\"><a href=\"/dashboard\">{{alumno.nombre}} {{alumno.apellido}}</a>
                                    </li>
                                    <!-- <li class=\"breadcrumb-item\"><a href=\"#\">Card</a>
                                    </li>
                                    <li class=\"breadcrumb-item active\">Advance Card
                                    </li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"content-body\">
              <div id=\"user-profile\">
                  <div class=\"row\">
                      <div class=\"col-12\">
                          <div class=\"profile-header mb-2\">
                              <div class=\"relative\">
                                  <div class=\"cover-container\">
                                      <img class=\"img-fluid bg-cover rounded-0 w-100\" src=\"../../../app-assets/images/profile/user-uploads/cover.png\" alt=\"User Profile Image\">
                                  </div>
                                  <div class=\"profile-img-container d-flex align-items-center justify-content-between\">
                                      <img src=\"{{alumno.businessUnit.logoUrl}}\" class=\"rounded-circle img-border box-shadow-1\" alt=\"Card image\">
                                      <!-- <div class=\"float-right\">
                                          <button type=\"button\" class=\"btn btn-icon btn-icon rounded-circle btn-primary mr-1\">
                                              <i class=\"feather icon-edit-2\"></i>
                                          </button>
                                          <button type=\"button\" class=\"btn btn-icon btn-icon rounded-circle btn-primary\">
                                              <i class=\"feather icon-settings\"></i>
                                          </button>
                                      </div> -->
                                  </div>
                              </div>
                              <div class=\"d-flex justify-content-end align-items-center profile-header-nav\">
                                  <!-- <nav class=\"navbar navbar-expand-sm w-100 pr-0\">
                                      <button class=\"navbar-toggler pr-0\" type=\"button\" data-toggle=\"collapse\" data-target=\"navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                          <span class=\"navbar-toggler-icon\"><i class=\"feather icon-align-justify\"></i></span>
                                      </button>
                                      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                          <ul class=\"navbar-nav justify-content-around w-75 ml-sm-auto\">
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Timeline</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">About</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Photos</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Friends</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Videos</a>
                                              </li>
                                              <li class=\"nav-item px-sm-0\">
                                                  <a href=\"#\" class=\"nav-link font-small-3\">Events</a>
                                              </li>
                                          </ul>
                                      </div>
                                  </nav> -->
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
              <!-- Timeline Starts -->
              <section id=\"timeline-card\">
                  <div class=\"row\">
                      <div class=\"col-lg-6 col-sm-12\">
                        <div class=\"card\">
                            <div class=\"card-header\">
                                <h4>{{alumno.nombre}} {{alumno.apellido}}</h4><br>
                            </div>
                            <div class=\"card-body\">
                                <!-- <p>Tart I love sugar plum I love oat cake. Sweet roll caramels I love jujubes. Topping cake wafer.</p> -->
                                <div class=\"mt-1\">
                                    <p><strong>Ciclo: </strong>{{alumno.cursos.ciclo.nombre}} <strong>Curso: </strong> {{alumno.cursos.nombre}} <strong>Division: </strong> {{alumno.cursos.division.nombre}}  </p>
                                </div>
                                <div class=\"mt-1\">
                                  <h6 class=\"mb-0\">Preceptores:</h6>
                                  {% for p in alumno.cursos.preceptores %}
                                  <p>{{p.nombre}} {{p.apellido}}</p>
                                  {% endfor %}
                                </div>
                                <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">DNI / Pasaporte:</h6>
                                    <p>{{alumno.dni}}</p>
                                </div>
                                <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">Padres:</h6>
                                    {% for p in alumno.padres %}
                                        <p>{{p.nombre}} {{p.apellido}}: <storng>Cel</strong> {{p.telefono}}
                                    {% endfor %}
                                    
                                </div>
                                <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">Direccion:</h6>
                                    <p>{{alumno.direccion}}</p>

                                </div>

                                <!-- <div class=\"mt-1\">
                                    <h6 class=\"mb-0\">Website:</h6>
                                    <p>www.pixinvent.com</p>
                                </div> -->
                                <!-- <div class=\"mt-1\">
                                    <button type=\"button\" class=\"btn btn-sm btn-icon btn-primary mr-25 p-25\"><i class=\"feather icon-facebook\"></i></button>
                                    <button type=\"button\" class=\"btn btn-sm btn-icon btn-primary mr-25 p-25\"><i class=\"feather icon-twitter\"></i></button>
                                    <button type=\"button\" class=\"btn btn-sm btn-icon btn-primary p-25\"><i class=\"feather icon-instagram\"></i></button>
                                </div> -->
                            </div>
                        </div>
                      </div>
                      <div class=\"col-lg-6 col-sm-12\">
                        <div class=\"card\">
                            <div class=\"card-header d-flex justify-content-between\">
                                <h4>Información</h4>
                                <i class=\"feather icon-more-horizontal cursor-pointer\"></i>
                            </div>

                            <div class=\"card-body\">
                            <div class=\"d-flex justify-content-start align-items-center mb-1\">

                                  <div class=\"user-page-info\">
                                      <h6 class=\"mb-0\">Información de la cursada</h6>
                                      <span class=\"font-small-2\">Materias, Horarios, Plan de estudios</span>
                                  </div>
                                  {% if alumno.cursos.horario != null %}
                                    <a href='/images/{{alumno.cursos.horario}}' class=\"btn btn-primary btn-icon ml-auto\"><i class=\"feather icon-file\"></i></a>
                                    {% endif %}
                            </div>
                              <h4>Materias</h4>
                              {% for m in alumno.cursos.materias %}
                                <div class=\"d-flex justify-content-start align-items-center mb-1\">

                                    <div class=\"user-page-info\">
                                        <h6 class=\"mb-0\">{{m.nombre}}</h6>
                                        <span class=\"font-small-2\">{{m.profesor.nombre}} {{m.profesor.apellido}}</span>
                                    </div>
                                      <!-- <button type=\"button\" class=\"btn btn-primary btn-icon ml-auto\"><i class=\"feather icon-file\"></i></button> -->
                                </div>
                                {% endfor %}
                            </div>
                        </div>
                        <!-- <div class=\"card\">

                            <div class=\"card-content\">
                              <div class=\"card-body text-center\">
                                  <h4>Cuaderno de comunicados</h4>
                                  <p class=\"\">Cuarto Grado</p>
                                  <div class=\"card-btns d-flex justify-content-center\">
                                      <button class=\"btn btn-outline-primary\">ver mas</button>
                                  </div>
                                  <div class=\"card-btns d-flex justify-content-center\">
                                      <button class=\"btn btn-outline-primary\">ver mas</button>
                                  </div>
                                  <div class=\"card-btns d-flex justify-content-center\">
                                      <button class=\"btn btn-outline-primary\">ver mas</button>
                                  </div>

                              </div>
                            </div>
                        </div> -->
                  </div>
              </section>
              <!-- Timeline Ends -->
              <div class=\"card-header\">
                  <h4>Cuaderno de comunicados</h4>

              </div>
              <!-- Basic example and Profile cards section start -->
              <section id=\"basic-examples\">
                  <div class=\"row match-height\">
                      <div class=\"col-xl-6 col-md-6 col-sm-12\">
                        <div class=\"card\">
                            <div class=\"card-content\">
                                <div class=\"card-body\">
                                    <!-- <img class=\"card-img img-fluid mb-1\" src=\"../../../app-assets/images/pages/content-img-3.jpg\" alt=\"Card image cap\"> -->
                                    <h5 class=\"my-1\">Comunicados</h5>
                                    <!-- <div class=\"d-flex justify-content-between\">
                                        <small class=\"float-left font-weight-bold mb-25\" id=\"example-caption-1\">\$ 5975</small>
                                        <small class=\"float-right  mb-25\" id=\"example-caption-2\">\$ 8000</small>
                                    </div> -->
                                    <div class=\"progress progress-bar-primary box-shadow-6\">
                                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"75\" aria-valuemax=\"100\" style=\"width:100%\" aria-describedby=\"example-caption-2\"></div>
                                    </div>
                                    <div class=\"card-btns d-flex justify-content-between\">
                                        <!-- <a href=\"#\" class=\"btn gradient-light-primary white\">Download</a> -->
                                        <a href=\"/comunicados?alumnoId={{alumno.id}}\" class=\"btn btn-outline-primary float-right\">Ver comunicados</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class=\"col-xl-6 col-md-6 col-sm-12\">
                          <div class=\"card\">
                              <div class=\"card-content\">
                                  <div class=\"card-body\">
                                      <!-- <img class=\"card-img img-fluid mb-1\" src=\"../../../app-assets/images/pages/content-img-3.jpg\" alt=\"Card image cap\"> -->
                                      <h5 class=\"my-1\">Mensajes</h5>
                                      <!-- <div class=\"d-flex justify-content-between\">
                                          <small class=\"float-left font-weight-bold mb-25\" id=\"example-caption-1\">\$ 5975</small>
                                          <small class=\"float-right  mb-25\" id=\"example-caption-2\">\$ 8000</small>
                                      </div> -->
                                      <div class=\"progress progress-bar-primary box-shadow-6\">
                                          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"75\" aria-valuemax=\"100\" style=\"width:100%\" aria-describedby=\"example-caption-2\"></div>
                                      </div>
                                      <div class=\"card-btns d-flex justify-content-between\">
                                          <!-- <a href=\"#\" class=\"btn gradient-light-primary white\">Download</a> -->
                                          <a href=\"/mensajes?alumnoId={{alumno.id}}\" class=\"btn btn-outline-primary float-right\">Ver mensajes</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- Profile Cards Starts -->






            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class=\"sidenav-overlay\"></div>
    <div class=\"drag-target\"></div>

    <!-- BEGIN: Footer-->
    <footer class=\"footer footer-static footer-light\">
        <p class=\"clearfix blue-grey lighten-2 mb-0\"><span class=\"float-md-left d-block d-md-inline-block mt-25\">Acricana - Cuaderno de Comunicados V0.1 - Powered by <a class=\"text-bold-800 grey darken-2\" href=\"#\" target=\"_blank\">Movius</a></span><span class=\"float-md-right d-none d-md-block\">Hand-crafted & Made with<i class=\"feather icon-heart pink\"></i></span>
            <button class=\"btn btn-primary btn-icon scroll-top\" type=\"button\"><i class=\"feather icon-arrow-up\"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src=\"../../../app-assets/js/scripts/pages/app-chat.js\"></script>
        <script src=\"../../../app-assets/js/scripts/pages/user-profile.js\"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
", "alumno.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/alumno.html.twig");
    }
}
