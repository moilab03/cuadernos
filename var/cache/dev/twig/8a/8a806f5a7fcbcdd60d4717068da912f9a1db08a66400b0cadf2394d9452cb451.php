<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* email/recuperar.html.twig */
class __TwigTemplate_41a939fe774de195a0a561996f8e51fc0296dde8edf4b71ab8ea0980b78526d4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/recuperar.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "email/recuperar.html.twig"));

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
<head>
\t<!--[if gte mso 9]>
\t<xml>
\t\t<o:OfficeDocumentSettings>
\t\t<o:AllowPNG/>
\t\t<o:PixelsPerInch>96</o:PixelsPerInch>
\t\t</o:OfficeDocumentSettings>
\t</xml>
\t<![endif]-->
\t<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
\t<meta name=\"format-detection\" content=\"date=no\" />
\t<meta name=\"format-detection\" content=\"address=no\" />
\t<meta name=\"format-detection\" content=\"telephone=no\" />
\t<meta name=\"x-apple-disable-message-reformatting\" />
\t<!--[if !mso]><!-->
\t<link href=\"https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&display=swap\" rel=\"stylesheet\" />
\t<!--<![endif]-->
\t<title>REPO</title>
\t<!--[if gte mso 9]>
\t<style type=\"text/css\" media=\"all\">
\t\tsup { font-size: 100% !important; }
\t</style>
\t<![endif]-->
\t<!-- body, html, table, thead, tbody, tr, td, div, a, span { font-family: Arial, sans-serif !important; } -->


\t<style type=\"text/css\" media=\"screen\">
\t\tbody { padding:0 !important; margin:0 auto !important; display:block !important; min-width:100% !important; width:100% !important; background:#f1f1f1; -webkit-text-size-adjust:none }
\t\ta { color:rgba(0, 106, 158, 1); text-decoration:none }
\t\tp { padding:0 !important; margin:0 !important }
\t\timg { margin: 0 !important; -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }

\t\ta[x-apple-data-detectors] { color: inherit !important; text-decoration: inherit !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }

\t\t.btn-16 a { display: block; padding: 15px 35px; text-decoration: none; }
\t\t.btn-20 a { display: block; padding: 15px 35px; text-decoration: none; }

\t\t.l-white a { color: #ffffff; }
\t\t.l-black a { color: #282828; }
\t\t.l-pink a { color: rgba(0, 106, 158, 1); }
\t\t.l-grey a { color: #6e6e6e; }
\t\t.l-purple a { color: rgba(0, 106, 158, 1); }

\t\t.gradient { background: linear-gradient(to right, rgba(0, 106, 158, 1) 0%,rgba(0, 106, 158, 1) 100%); }

\t\t.btn-secondary { border-radius: 10px; background: linear-gradient(to right, rgba(0, 106, 158, 1) 0%,rgba(0, 106, 158, 1) 100%); }


\t\t/* Mobile styles */
\t\t@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
\t\t\t.mpx-10 { padding-left: 10px !important; padding-right: 10px !important; }

\t\t\t.mpx-15 { padding-left: 15px !important; padding-right: 15px !important; }

\t\t\tu + .body .gwfw { width:100% !important; width:100vw !important; }

\t\t\t.td,
\t\t\t.m-shell { width: 100% !important; min-width: 100% !important; }

\t\t\t.mt-left { text-align: left !important; }
\t\t\t.mt-center { text-align: center !important; }
\t\t\t.mt-right { text-align: right !important; }

\t\t\t.me-left { margin-right: auto !important; }
\t\t\t.me-center { margin: 0 auto !important; }
\t\t\t.me-right { margin-left: auto !important; }

\t\t\t.mh-auto { height: auto !important; }
\t\t\t.mw-auto { width: auto !important; }

\t\t\t.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

\t\t\t.column,
\t\t\t.column-top,
\t\t\t.column-dir-top { float: left !important; width: 100% !important; display: block !important; }

\t\t\t.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
\t\t\t.m-block { display: block !important; }

\t\t\t.mw-15 { width: 15px !important; }

\t\t\t.mw-2p { width: 2% !important; }
\t\t\t.mw-32p { width: 32% !important; }
\t\t\t.mw-49p { width: 49% !important; }
\t\t\t.mw-50p { width: 50% !important; }
\t\t\t.mw-100p { width: 100% !important; }

\t\t\t.mmt-0 { margin-top: 0 !important; }
\t\t}

\t\t\t</style>
</head>
<body class=\"body\" style=\"padding:0 !important; margin:0 auto !important; display:block !important; min-width:100% !important; width:100% !important; background:#f1f1f1; -webkit-text-size-adjust:none;\">
\t<center>
\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin: 0; padding: 0; width: 100%; height: 100%;\" bgcolor=\"#f1f1f1\" class=\"gwfw\">
\t\t\t<tr>
\t\t\t\t<td style=\"margin: 0; padding: 0; width: 100%; height: 100%;\" align=\"center\" valign=\"top\">
\t\t\t\t\t<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m-shell\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td class=\"td\" style=\"width:600px; min-width:600px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\">
\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"mpx-10\">
\t\t\t\t\t\t\t\t\t\t\t<!-- Top -->
\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-12 c-grey l-grey a-right py-20\" style=\"font-size:12px; line-height:16px; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; color:#6e6e6e; text-align:right; padding-top: 20px; padding-bottom: 20px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\">View this email in your browser</span></a> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t</table>\t\t\t\t\t\t\t\t\t\t\t<!-- END Top -->

\t\t\t\t\t\t\t\t\t\t\t<!-- Container -->
\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"gradient pt-10\" style=\"border-radius: 10px 10px 0 0; padding-top: 10px;\" bgcolor=\"rgba(0, 106, 158, 1)\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"border-radius: 10px 10px 0 0;\" bgcolor=\"#ffffff\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Logo -->


\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Main -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"px-50 mpx-15\" style=\"padding-left: 50px; padding-right: 50px; padding-top: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Section - Intro -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"pb-50\" style=\"padding-bottom: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"fluid-img img-center pb-50\" style=\"font-size:0pt; line-height:0pt; text-align:center; padding-bottom: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 138
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new RuntimeError('Variable "base_url" does not exist.', 138, $this->source); })()), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["logo"]) || array_key_exists("logo", $context) ? $context["logo"] : (function () { throw new RuntimeError('Variable "logo" does not exist.', 138, $this->source); })()), "html", null, true);
        echo "\" width=\"196\" height=\"136\" border=\"0\" alt=\"\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"img-center p-30 px-15\" style=\"font-size:0pt; line-height:0pt; text-align:center; padding: 30px; padding-left: 15px; padding-right: 15px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" target=\"_blank\"><img src=\"https://repo.vivamoscomodoro.gob.ar/assets/img/cr-vc.png\" width=\"auto\" height=\"auto\" border=\"0\" alt=\"\" /></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"title-36 a-center pb-15\" style=\"font-size:36px; line-height:40px; color:#282828; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; text-align:center; padding-bottom: 15px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong>Recuperar contraseña</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-16 lh-26 a-center pb-25\" style=\"font-size:16px; color:#6e6e6e; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; line-height: 26px; text-align:center; padding-bottom: 25px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tHola! ";
        // line 156
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 156, $this->source); })()), "nombre", [], "any", false, false, false, 156), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 156, $this->source); })()), "apellido", [], "any", false, false, false, 156), "html", null, true);
        echo "<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tRecibimos una notificación de que estás intentando recuperar tu contraseña<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tSeguí los pasos haciendo click en el siguiente botón.
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Button -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"min-width: 200px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"btn-16 c-white l-white\" bgcolor=\"rgba(0, 106, 158, 1)\" style=\"font-size:16px; line-height:20px; mso-padding-alt:15px 35px; font-family:'PT Sans', Arial, sans-serif; text-align:center; font-weight:bold; text-transform:uppercase; border-radius:25px; min-width:auto !important; color:#ffffff;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 170
        echo twig_escape_filter($this->env, (isset($context["base_url"]) || array_key_exists("base_url", $context) ? $context["base_url"] : (function () { throw new RuntimeError('Variable "base_url" does not exist.', 170, $this->source); })()), "html", null, true);
        echo "/recuperar/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 170, $this->source); })()), "confirmationToken", [], "any", false, false, false, 170), "html", null, true);
        echo "\" target=\"_blank\" class=\"link c-white\" style=\"display: block; padding: 15px 35px; text-decoration:none; color:#ffffff;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"link c-white\" style=\"text-decoration:none; color:#ffffff;\">Recuperar contraseña</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Button -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Section - Intro -->

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Section - Separator Line -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"pb-50\" style=\"padding-bottom: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <td class=\"img\" height=\"1\" bgcolor=\"#ebebeb\" style=\"font-size:0pt; line-height:0pt; text-align:left;\">&nbsp;</td> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Section - Separator Line -->


\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Main -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t<!-- END Container -->

\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<!-- Bottom -->
\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-12 lh-22 a-center c-grey- l-grey py-20\" style=\"font-size:12px; color:#6e6e6e; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; line-height: 22px; text-align:center; padding-top: 20px; padding-bottom: 20px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"white-space: nowrap; text-decoration:none; color:#6e6e6e;\">UNSUBSCRIBE</span></a> &nbsp;|&nbsp; <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"white-space: nowrap; text-decoration:none; color:#6e6e6e;\">WEB VERSION</span></a> &nbsp;|&nbsp; <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"white-space: nowrap; text-decoration:none; color:#6e6e6e;\">SEND TO A FRIEND</span></a> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Bottom -->
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</table>
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</center>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "email/recuperar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 170,  204 => 156,  182 => 138,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
<head>
\t<!--[if gte mso 9]>
\t<xml>
\t\t<o:OfficeDocumentSettings>
\t\t<o:AllowPNG/>
\t\t<o:PixelsPerInch>96</o:PixelsPerInch>
\t\t</o:OfficeDocumentSettings>
\t</xml>
\t<![endif]-->
\t<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
\t<meta name=\"format-detection\" content=\"date=no\" />
\t<meta name=\"format-detection\" content=\"address=no\" />
\t<meta name=\"format-detection\" content=\"telephone=no\" />
\t<meta name=\"x-apple-disable-message-reformatting\" />
\t<!--[if !mso]><!-->
\t<link href=\"https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&display=swap\" rel=\"stylesheet\" />
\t<!--<![endif]-->
\t<title>REPO</title>
\t<!--[if gte mso 9]>
\t<style type=\"text/css\" media=\"all\">
\t\tsup { font-size: 100% !important; }
\t</style>
\t<![endif]-->
\t<!-- body, html, table, thead, tbody, tr, td, div, a, span { font-family: Arial, sans-serif !important; } -->


\t<style type=\"text/css\" media=\"screen\">
\t\tbody { padding:0 !important; margin:0 auto !important; display:block !important; min-width:100% !important; width:100% !important; background:#f1f1f1; -webkit-text-size-adjust:none }
\t\ta { color:rgba(0, 106, 158, 1); text-decoration:none }
\t\tp { padding:0 !important; margin:0 !important }
\t\timg { margin: 0 !important; -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }

\t\ta[x-apple-data-detectors] { color: inherit !important; text-decoration: inherit !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }

\t\t.btn-16 a { display: block; padding: 15px 35px; text-decoration: none; }
\t\t.btn-20 a { display: block; padding: 15px 35px; text-decoration: none; }

\t\t.l-white a { color: #ffffff; }
\t\t.l-black a { color: #282828; }
\t\t.l-pink a { color: rgba(0, 106, 158, 1); }
\t\t.l-grey a { color: #6e6e6e; }
\t\t.l-purple a { color: rgba(0, 106, 158, 1); }

\t\t.gradient { background: linear-gradient(to right, rgba(0, 106, 158, 1) 0%,rgba(0, 106, 158, 1) 100%); }

\t\t.btn-secondary { border-radius: 10px; background: linear-gradient(to right, rgba(0, 106, 158, 1) 0%,rgba(0, 106, 158, 1) 100%); }


\t\t/* Mobile styles */
\t\t@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
\t\t\t.mpx-10 { padding-left: 10px !important; padding-right: 10px !important; }

\t\t\t.mpx-15 { padding-left: 15px !important; padding-right: 15px !important; }

\t\t\tu + .body .gwfw { width:100% !important; width:100vw !important; }

\t\t\t.td,
\t\t\t.m-shell { width: 100% !important; min-width: 100% !important; }

\t\t\t.mt-left { text-align: left !important; }
\t\t\t.mt-center { text-align: center !important; }
\t\t\t.mt-right { text-align: right !important; }

\t\t\t.me-left { margin-right: auto !important; }
\t\t\t.me-center { margin: 0 auto !important; }
\t\t\t.me-right { margin-left: auto !important; }

\t\t\t.mh-auto { height: auto !important; }
\t\t\t.mw-auto { width: auto !important; }

\t\t\t.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

\t\t\t.column,
\t\t\t.column-top,
\t\t\t.column-dir-top { float: left !important; width: 100% !important; display: block !important; }

\t\t\t.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
\t\t\t.m-block { display: block !important; }

\t\t\t.mw-15 { width: 15px !important; }

\t\t\t.mw-2p { width: 2% !important; }
\t\t\t.mw-32p { width: 32% !important; }
\t\t\t.mw-49p { width: 49% !important; }
\t\t\t.mw-50p { width: 50% !important; }
\t\t\t.mw-100p { width: 100% !important; }

\t\t\t.mmt-0 { margin-top: 0 !important; }
\t\t}

\t\t\t</style>
</head>
<body class=\"body\" style=\"padding:0 !important; margin:0 auto !important; display:block !important; min-width:100% !important; width:100% !important; background:#f1f1f1; -webkit-text-size-adjust:none;\">
\t<center>
\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin: 0; padding: 0; width: 100%; height: 100%;\" bgcolor=\"#f1f1f1\" class=\"gwfw\">
\t\t\t<tr>
\t\t\t\t<td style=\"margin: 0; padding: 0; width: 100%; height: 100%;\" align=\"center\" valign=\"top\">
\t\t\t\t\t<table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"m-shell\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td class=\"td\" style=\"width:600px; min-width:600px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;\">
\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td class=\"mpx-10\">
\t\t\t\t\t\t\t\t\t\t\t<!-- Top -->
\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-12 c-grey l-grey a-right py-20\" style=\"font-size:12px; line-height:16px; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; color:#6e6e6e; text-align:right; padding-top: 20px; padding-bottom: 20px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\">View this email in your browser</span></a> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t</table>\t\t\t\t\t\t\t\t\t\t\t<!-- END Top -->

\t\t\t\t\t\t\t\t\t\t\t<!-- Container -->
\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"gradient pt-10\" style=\"border-radius: 10px 10px 0 0; padding-top: 10px;\" bgcolor=\"rgba(0, 106, 158, 1)\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"border-radius: 10px 10px 0 0;\" bgcolor=\"#ffffff\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Logo -->


\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Main -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"px-50 mpx-15\" style=\"padding-left: 50px; padding-right: 50px; padding-top: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Section - Intro -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"pb-50\" style=\"padding-bottom: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"fluid-img img-center pb-50\" style=\"font-size:0pt; line-height:0pt; text-align:center; padding-bottom: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"{{base_url}}{{logo}}\" width=\"196\" height=\"136\" border=\"0\" alt=\"\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"img-center p-30 px-15\" style=\"font-size:0pt; line-height:0pt; text-align:center; padding: 30px; padding-left: 15px; padding-right: 15px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" target=\"_blank\"><img src=\"https://repo.vivamoscomodoro.gob.ar/assets/img/cr-vc.png\" width=\"auto\" height=\"auto\" border=\"0\" alt=\"\" /></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"title-36 a-center pb-15\" style=\"font-size:36px; line-height:40px; color:#282828; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; text-align:center; padding-bottom: 15px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong>Recuperar contraseña</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-16 lh-26 a-center pb-25\" style=\"font-size:16px; color:#6e6e6e; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; line-height: 26px; text-align:center; padding-bottom: 25px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tHola! {{user.nombre}} {{user.apellido}}<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tRecibimos una notificación de que estás intentando recuperar tu contraseña<br>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tSeguí los pasos haciendo click en el siguiente botón.
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Button -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"min-width: 200px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"btn-16 c-white l-white\" bgcolor=\"rgba(0, 106, 158, 1)\" style=\"font-size:16px; line-height:20px; mso-padding-alt:15px 35px; font-family:'PT Sans', Arial, sans-serif; text-align:center; font-weight:bold; text-transform:uppercase; border-radius:25px; min-width:auto !important; color:#ffffff;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{base_url}}/recuperar/{{user.confirmationToken}}\" target=\"_blank\" class=\"link c-white\" style=\"display: block; padding: 15px 35px; text-decoration:none; color:#ffffff;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"link c-white\" style=\"text-decoration:none; color:#ffffff;\">Recuperar contraseña</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Button -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Section - Intro -->

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- Section - Separator Line -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"pb-50\" style=\"padding-bottom: 50px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <td class=\"img\" height=\"1\" bgcolor=\"#ebebeb\" style=\"font-size:0pt; line-height:0pt; text-align:left;\">&nbsp;</td> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Section - Separator Line -->


\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Main -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t<!-- END Container -->

\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<!-- Bottom -->
\t\t\t\t\t\t\t\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-12 lh-22 a-center c-grey- l-grey py-20\" style=\"font-size:12px; color:#6e6e6e; font-family:'PT Sans', Arial, sans-serif; min-width:auto !important; line-height: 22px; text-align:center; padding-top: 20px; padding-bottom: 20px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"white-space: nowrap; text-decoration:none; color:#6e6e6e;\">UNSUBSCRIBE</span></a> &nbsp;|&nbsp; <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"white-space: nowrap; text-decoration:none; color:#6e6e6e;\">WEB VERSION</span></a> &nbsp;|&nbsp; <a href=\"#\" target=\"_blank\" class=\"link c-grey\" style=\"text-decoration:none; color:#6e6e6e;\"><span class=\"link c-grey\" style=\"white-space: nowrap; text-decoration:none; color:#6e6e6e;\">SEND TO A FRIEND</span></a> -->
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<!-- END Bottom -->
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</table>
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t</center>
</body>
</html>
", "email/recuperar.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/email/recuperar.html.twig");
    }
}
