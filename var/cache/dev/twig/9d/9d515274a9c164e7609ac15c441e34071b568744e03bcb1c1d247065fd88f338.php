<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* menu.html.twig */
class __TwigTemplate_8ede53ae90b9e1b19e148c75beb530ad7d8b67e17ee456c9b6b92f385a3793ca extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "menu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "menu.html.twig"));

        // line 1
        echo "<!-- BEGIN: Header-->

";
        // line 3
        $context["user"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 3, $this->source); })()), "session", [], "any", false, false, false, 3), "get", [0 => "user"], "method", false, false, false, 3);
        // line 4
        $context["alumnos"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "session", [], "any", false, false, false, 4), "get", [0 => "alumnos"], "method", false, false, false, 4);
        // line 5
        $context["logoUrl"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "session", [], "any", false, false, false, 5), "get", [0 => "logoUrl"], "method", false, false, false, 5);
        // line 6
        $context["bu"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 6, $this->source); })()), "session", [], "any", false, false, false, 6), "get", [0 => "bu"], "method", false, false, false, 6);
        // line 7
        echo "  <nav class=\"header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav  navbar-shadow\">
      <div class=\"navbar-wrapper\">
          <div class=\"navbar-container content\">
              <div class=\"navbar-collapse\" id=\"navbar-mobile\">
                  <div class=\"mr-auto float-left bookmark-wrapper d-flex align-items-center\">
                      <ul class=\"nav navbar-nav\">
                          <li class=\"nav-item mobile-menu d-xl-none mr-auto\"><a class=\"nav-link nav-menu-main menu-toggle hidden-xs\" href=\"#\"><i class=\"ficon feather icon-menu\"></i></a></li>
                      </ul>


                  </div>
                  <ul class=\"nav navbar-nav float-right\">

                      ";
        // line 21
        echo "                          <ul class=\"dropdown-menu dropdown-menu-media dropdown-menu-right\">
                              <li class=\"dropdown-menu-header\">
                                  <div class=\"dropdown-header m-0 p-2\">
                                      <h3 class=\"white\">2 Nuevas</h3><span class=\"notification-title\">Notificaciones</span>
                                  </div>
                              </li>
                              <li class=\"scrollable-container media-list\"><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">

                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">

                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">
                                      <div class=\"media d-flex align-items-start\">
                                          <div class=\"media-left\"><i class=\"feather icon-alert-triangle font-medium-5 danger\"></i></div>
                                          <div class=\"media-body\">
                                              <h6 class=\"danger media-heading yellow darken-3\">Llamado de atención</h6></div><small>
                                              <time class=\"media-meta\" datetime=\"2015-06-11T18:29:20+08:00\">Hoy</time></small>
                                      </div>
                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">
                                      <div class=\"media d-flex align-items-start\">
                                          <div class=\"media-left\"><i class=\"feather icon-check-circle font-medium-5 info\"></i></div>
                                          <div class=\"media-body\">
                                              <h6 class=\"info media-heading\">Nuevo comunicado</h6>
                                          </div><small>
                                              <time class=\"media-meta\" datetime=\"2015-06-11T18:29:20+08:00\">Semana pasada</time></small>
                                      </div>
                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">

                                  </a></li>

                          </ul>
                      </li>
                      <li class=\"dropdown dropdown-user nav-item\"><a class=\"dropdown-toggle nav-link dropdown-user-link\" href=\"#\" data-toggle=\"dropdown\">
                              <div class=\"user-nav d-sm-flex d-none\"><span class=\"user-name text-bold-600\">";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 53, $this->source); })()), "usernameCanonical", [], "any", false, false, false, 53), "html", null, true);
        echo "</span></div><span>  <div class=\"avatar mr-1\">
                                    <span class=\"avatar-content\">FF</span>
                                </div></span>
                          </a>
                          <div class=\"dropdown-menu dropdown-menu-right\">
                          ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["alumnos"]) || array_key_exists("alumnos", $context) ? $context["alumnos"] : (function () { throw new RuntimeError('Variable "alumnos" does not exist.', 58, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 59
            echo "                            <a class=\"dropdown-item\" href=\"/alumno/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "id", [], "any", false, false, false, 59), "html", null, true);
            echo "\"><i class=\"feather icon-user\"></i>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "nombre", [], "any", false, false, false, 59), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "apellido", [], "any", false, false, false, 59), "html", null, true);
            echo "</a>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                            
                            <a class=\"dropdown-item\" href=\"/cerrar-session\"><i class=\"feather icon-power\"></i> Logout</a>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </nav>

  <!-- END: Header-->


  <!-- BEGIN: Main Menu-->
  <div class=\"main-menu menu-fixed menu-light menu-accordion menu-shadow\" data-scroll-to-active=\"true\">
      <div class=\"navbar-header\">
          <ul class=\"nav navbar-nav flex-row\">
              <li class=\"nav-item mr-auto\"><a class=\"navbar-brand\" href=\"/dashboard\">
                      <img src=\"";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["logoUrl"]) || array_key_exists("logoUrl", $context) ? $context["logoUrl"] : (function () { throw new RuntimeError('Variable "logoUrl" does not exist.', 79, $this->source); })()), "html", null, true);
        echo "\" style=\"height: 60px\"/>
                      <h2 class=\"brand-text mb-0\">";
        // line 80
        echo ((((isset($context["bu"]) || array_key_exists("bu", $context) ? $context["bu"] : (function () { throw new RuntimeError('Variable "bu" does not exist.', 80, $this->source); })()) == 1)) ? ("ACRICANA") : ("ALS"));
        echo "</h2>
                  </a></li>
              <li class=\"nav-item nav-toggle\"><a class=\"nav-link modern-nav-toggle pr-0\" data-toggle=\"collapse\"><i class=\"feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon\"></i><i class=\"toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary\" data-ticon=\"icon-disc\"></i></a></li>
          </ul>
      </div>
      <div class=\"shadow-bottom\"></div>
      <div class=\"main-menu-content\">
          <ul class=\"navigation navigation-main\" id=\"main-menu-navigation\" data-menu=\"menu-navigation\">
              <li class=\" nav-item\"><a href=\"/dashboard\"><i class=\"feather icon-home\"></i><span class=\"menu-title\" data-i18n=\"Dashboard\">Dashboard</span></a>
                  
              </li>
              <li class=\" navigation-header\"><span>Apps</span>
              </li>
              <li class=\" nav-item\"><a href=\"/comunicados\"><i class=\"feather icon-check-square\"></i><span class=\"menu-title\" data-i18n=\"Email\">Comunicados</span></a>
              </li>
              <li class=\" nav-item\"><a href=\"/mensajes\"><i class=\"feather icon-mail\"></i><span class=\"menu-title\" data-i18n=\"Todo\">Mensajes</span></a>
              </li>

            <!-- <li class=\" nav-item\"><a href=\"#\"><i class=\"feather icon-unlock\"></i><span class=\"menu-title\" data-i18n=\"Authentication\">Authentication</span></a>
                  <ul class=\"menu-content\">
                      <li><a href=\"auth-login.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Login\">Login</span></a>
                      </li>
                      <li><a href=\"auth-register.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Register\">Register</span></a>
                      </li>
                      <li><a href=\"auth-forgot-password.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Forgot Password\">Forgot Password</span></a>
                      </li>
                      <li><a href=\"auth-reset-password.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Reset Password\">Reset Password</span></a>
                      </li>
                      <li><a href=\"auth-lock-screen.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Lock Screen\">Lock Screen</span></a>
                      </li>
                  </ul>
              </li> -->

          </ul>
      </div>
  </div>

  <div class=\"alert alert-warning alert-dismissible fade show\" role=\"alert\">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
    <span aria-hidden=\"true\">&times;</span>
  </button>
</div>
  <!-- END: Main Menu-->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 80,  149 => 79,  129 => 61,  116 => 59,  112 => 58,  104 => 53,  70 => 21,  55 => 7,  53 => 6,  51 => 5,  49 => 4,  47 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- BEGIN: Header-->

{% set user = app.session.get('user') %}
{% set alumnos = app.session.get('alumnos') %}
{% set logoUrl = app.session.get('logoUrl') %}
{% set bu = app.session.get('bu') %}
  <nav class=\"header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav  navbar-shadow\">
      <div class=\"navbar-wrapper\">
          <div class=\"navbar-container content\">
              <div class=\"navbar-collapse\" id=\"navbar-mobile\">
                  <div class=\"mr-auto float-left bookmark-wrapper d-flex align-items-center\">
                      <ul class=\"nav navbar-nav\">
                          <li class=\"nav-item mobile-menu d-xl-none mr-auto\"><a class=\"nav-link nav-menu-main menu-toggle hidden-xs\" href=\"#\"><i class=\"ficon feather icon-menu\"></i></a></li>
                      </ul>


                  </div>
                  <ul class=\"nav navbar-nav float-right\">

                      {# <li class=\"dropdown dropdown-notification nav-item\"><a class=\"nav-link nav-link-label\" href=\"#\" data-toggle=\"dropdown\"><i class=\"ficon feather icon-bell\"></i><span class=\"badge badge-pill badge-primary badge-up\">2</span></a> #}
                          <ul class=\"dropdown-menu dropdown-menu-media dropdown-menu-right\">
                              <li class=\"dropdown-menu-header\">
                                  <div class=\"dropdown-header m-0 p-2\">
                                      <h3 class=\"white\">2 Nuevas</h3><span class=\"notification-title\">Notificaciones</span>
                                  </div>
                              </li>
                              <li class=\"scrollable-container media-list\"><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">

                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">

                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">
                                      <div class=\"media d-flex align-items-start\">
                                          <div class=\"media-left\"><i class=\"feather icon-alert-triangle font-medium-5 danger\"></i></div>
                                          <div class=\"media-body\">
                                              <h6 class=\"danger media-heading yellow darken-3\">Llamado de atención</h6></div><small>
                                              <time class=\"media-meta\" datetime=\"2015-06-11T18:29:20+08:00\">Hoy</time></small>
                                      </div>
                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">
                                      <div class=\"media d-flex align-items-start\">
                                          <div class=\"media-left\"><i class=\"feather icon-check-circle font-medium-5 info\"></i></div>
                                          <div class=\"media-body\">
                                              <h6 class=\"info media-heading\">Nuevo comunicado</h6>
                                          </div><small>
                                              <time class=\"media-meta\" datetime=\"2015-06-11T18:29:20+08:00\">Semana pasada</time></small>
                                      </div>
                                  </a><a class=\"d-flex justify-content-between\" href=\"javascript:void(0)\">

                                  </a></li>

                          </ul>
                      </li>
                      <li class=\"dropdown dropdown-user nav-item\"><a class=\"dropdown-toggle nav-link dropdown-user-link\" href=\"#\" data-toggle=\"dropdown\">
                              <div class=\"user-nav d-sm-flex d-none\"><span class=\"user-name text-bold-600\">{{user.usernameCanonical}}</span></div><span>  <div class=\"avatar mr-1\">
                                    <span class=\"avatar-content\">FF</span>
                                </div></span>
                          </a>
                          <div class=\"dropdown-menu dropdown-menu-right\">
                          {% for a in alumnos %}
                            <a class=\"dropdown-item\" href=\"/alumno/{{a.id}}\"><i class=\"feather icon-user\"></i>{{a.nombre}} {{a.apellido}}</a>
                            {% endfor %}
                            
                            <a class=\"dropdown-item\" href=\"/cerrar-session\"><i class=\"feather icon-power\"></i> Logout</a>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </nav>

  <!-- END: Header-->


  <!-- BEGIN: Main Menu-->
  <div class=\"main-menu menu-fixed menu-light menu-accordion menu-shadow\" data-scroll-to-active=\"true\">
      <div class=\"navbar-header\">
          <ul class=\"nav navbar-nav flex-row\">
              <li class=\"nav-item mr-auto\"><a class=\"navbar-brand\" href=\"/dashboard\">
                      <img src=\"{{logoUrl}}\" style=\"height: 60px\"/>
                      <h2 class=\"brand-text mb-0\">{{ bu == 1 ? 'ACRICANA' : 'ALS' }}</h2>
                  </a></li>
              <li class=\"nav-item nav-toggle\"><a class=\"nav-link modern-nav-toggle pr-0\" data-toggle=\"collapse\"><i class=\"feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon\"></i><i class=\"toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary\" data-ticon=\"icon-disc\"></i></a></li>
          </ul>
      </div>
      <div class=\"shadow-bottom\"></div>
      <div class=\"main-menu-content\">
          <ul class=\"navigation navigation-main\" id=\"main-menu-navigation\" data-menu=\"menu-navigation\">
              <li class=\" nav-item\"><a href=\"/dashboard\"><i class=\"feather icon-home\"></i><span class=\"menu-title\" data-i18n=\"Dashboard\">Dashboard</span></a>
                  
              </li>
              <li class=\" navigation-header\"><span>Apps</span>
              </li>
              <li class=\" nav-item\"><a href=\"/comunicados\"><i class=\"feather icon-check-square\"></i><span class=\"menu-title\" data-i18n=\"Email\">Comunicados</span></a>
              </li>
              <li class=\" nav-item\"><a href=\"/mensajes\"><i class=\"feather icon-mail\"></i><span class=\"menu-title\" data-i18n=\"Todo\">Mensajes</span></a>
              </li>

            <!-- <li class=\" nav-item\"><a href=\"#\"><i class=\"feather icon-unlock\"></i><span class=\"menu-title\" data-i18n=\"Authentication\">Authentication</span></a>
                  <ul class=\"menu-content\">
                      <li><a href=\"auth-login.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Login\">Login</span></a>
                      </li>
                      <li><a href=\"auth-register.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Register\">Register</span></a>
                      </li>
                      <li><a href=\"auth-forgot-password.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Forgot Password\">Forgot Password</span></a>
                      </li>
                      <li><a href=\"auth-reset-password.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Reset Password\">Reset Password</span></a>
                      </li>
                      <li><a href=\"auth-lock-screen.html\"><i class=\"feather icon-circle\"></i><span class=\"menu-item\" data-i18n=\"Lock Screen\">Lock Screen</span></a>
                      </li>
                  </ul>
              </li> -->

          </ul>
      </div>
  </div>

  <div class=\"alert alert-warning alert-dismissible fade show\" role=\"alert\">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
    <span aria-hidden=\"true\">&times;</span>
  </button>
</div>
  <!-- END: Main Menu-->", "menu.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/menu.html.twig");
    }
}
