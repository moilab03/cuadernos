<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index-acricana.html.twig */
class __TwigTemplate_5235a7dff0c4cdb2100a50e9a13e44d2dd3939ed2b77e0057887535461467103 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index-acricana.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index-acricana.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Acricana</title>
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/dashboard-ecommerce.css\">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../assets/css/style.css\">
   <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js\"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js\"></script>

  <!-- Add Firebase products that you want to use -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js\"></script>
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js\"></script>
  ";
        // line 45
        $context["user"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 45, $this->source); })()), "session", [], "any", false, false, false, 45), "get", [0 => "user"], "method", false, false, false, 45);
        // line 46
        echo "  <script> 
  var userId = ";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 47, $this->source); })()), "id", [], "any", false, false, false, 47), "html", null, true);
        echo "; 
  </script>
 <script src=\"firebase-messaging-sw.js\"></script>

 <!-- JavaScript -->
<script src=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js\"></script>

<!-- CSS -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css\"/>
<!-- Default theme -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css\"/>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern dark-layout 2-columns  navbar-floating footer-static  \" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"2-columns\" data-layout=\"dark-layout\">

    ";
        // line 66
        $this->loadTemplate("menu.html.twig", "index-acricana.html.twig", 66)->display($context);
        // line 67
        echo "
    <!-- BEGIN: Content-->
    <div class=\"app-content content\">
        <div class=\"content-overlay\"></div>
        <div class=\"header-navbar-shadow\"></div>
        <div class=\"content-wrapper\">
            <div class=\"content-header row\">
                <div class=\"content-header-left col-md-9 col-12 mb-2\">
                    <div class=\"row breadcrumbs-top\">
                        <div class=\"col-12\">
                            <h2 class=\"content-header-title float-left mb-0\">Dashboard</h2>
                            <div class=\"breadcrumb-wrapper col-12\">
                                <ol class=\"breadcrumb\">
                                    <li class=\"breadcrumb-item\"><a href=\"inicio.html\">Inicio</a>
                                    </li>
                                    <!-- <li class=\"breadcrumb-item\"><a href=\"#\">Card</a>
                                    </li>
                                    <li class=\"breadcrumb-item active\">Advance Card
                                    </li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"content-body\">
              <!-- Timeline Starts -->
              <section id=\"timeline-card\">
                  <div class=\"row\">
                    ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["alumnos"]) || array_key_exists("alumnos", $context) ? $context["alumnos"] : (function () { throw new RuntimeError('Variable "alumnos" does not exist.', 97, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 98
            echo "                        <div class=\"col-lg-6 col-sm-12\">
                            <div class=\"card\">

                                <div class=\"card-content\">
                                    <div class=\"card-body text-center\">
                                        <h4>";
            // line 103
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "nombre", [], "any", false, false, false, 103), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "apellido", [], "any", false, false, false, 103), "html", null, true);
            echo "</h4>
                                        <p class=\"\">";
            // line 104
            (((twig_get_attribute($this->env, $this->source, $context["a"], "curso", [], "any", false, false, false, 104) != null)) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["a"], "curso", [], "any", false, false, false, 104), "nombre", [], "any", false, false, false, 104), "html", null, true))) : (print ("")));
            echo "</p>
                                        <div class=\"card-btns d-flex justify-content-center\">
                                            <a class=\"btn btn-outline-primary\" href=\"/alumno/";
            // line 106
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "id", [], "any", false, false, false, 106), "html", null, true);
            echo "\">Ver mas</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "                  </div>
              </section>
              <!-- Timeline Ends -->






            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class=\"sidenav-overlay\"></div>
    <div class=\"drag-target\"></div>

    <!-- BEGIN: Footer-->
    <footer class=\"footer footer-static footer-light\">
        <p class=\"clearfix blue-grey lighten-2 mb-0\"><span class=\"float-md-left d-block d-md-inline-block mt-25\">Acricana - Cuaderno de Comunicados V0.1 - Powered by <a class=\"text-bold-800 grey darken-2\" href=\"#\" target=\"_blank\">Movius</a></span><span class=\"float-md-right d-none d-md-block\">Hand-crafted & Made with<i class=\"feather icon-heart pink\"></i></span>
            <button class=\"btn btn-primary btn-icon scroll-top\" type=\"button\"><i class=\"feather icon-arrow-up\"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src=\"../../../app-assets/js/scripts/pages/app-chat.js\"></script>
    <script src=\"/firebase/index.js\"></script>
    <script src=\"/notify.js\"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "index-acricana.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 114,  172 => 106,  167 => 104,  161 => 103,  154 => 98,  150 => 97,  118 => 67,  116 => 66,  94 => 47,  91 => 46,  89 => 45,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html class=\"loading\" lang=\"en\" data-textdirection=\"ltr\">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui\">
    <meta name=\"description\" content=\"Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.\">
    <meta name=\"keywords\" content=\"admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app\">
    <meta name=\"author\" content=\"PIXINVENT\">
    <title>Acricana</title>
    <link rel=\"apple-touch-icon\" href=\"../../../app-assets/images/ico/apple-icon-120.png\">
    <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"../../../app-assets/images/ico/favicon.ico\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600\" rel=\"stylesheet\">

    <!-- BEGIN: Vendor CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/vendors/css/vendors.min.css\">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/bootstrap-extended.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/colors.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/components.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/layout.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/themes/semi-dark-layout.css\">

    <!-- BEGIN: Page CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/menu/menu-types/vertical-menu.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/core/colors/palette-gradient.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../app-assets/css/pages/dashboard-ecommerce.css\">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../../../assets/css/style.css\">
   <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js\"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js\"></script>

  <!-- Add Firebase products that you want to use -->
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-auth.js\"></script>
  <script src=\"https://www.gstatic.com/firebasejs/8.9.1/firebase-firestore.js\"></script>
  {% set user = app.session.get('user') %}
  <script> 
  var userId = {{user.id}}; 
  </script>
 <script src=\"firebase-messaging-sw.js\"></script>

 <!-- JavaScript -->
<script src=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js\"></script>

<!-- CSS -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css\"/>
<!-- Default theme -->
<link rel=\"stylesheet\" href=\"//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css\"/>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class=\"vertical-layout vertical-menu-modern dark-layout 2-columns  navbar-floating footer-static  \" data-open=\"click\" data-menu=\"vertical-menu-modern\" data-col=\"2-columns\" data-layout=\"dark-layout\">

    {% include 'menu.html.twig' %}

    <!-- BEGIN: Content-->
    <div class=\"app-content content\">
        <div class=\"content-overlay\"></div>
        <div class=\"header-navbar-shadow\"></div>
        <div class=\"content-wrapper\">
            <div class=\"content-header row\">
                <div class=\"content-header-left col-md-9 col-12 mb-2\">
                    <div class=\"row breadcrumbs-top\">
                        <div class=\"col-12\">
                            <h2 class=\"content-header-title float-left mb-0\">Dashboard</h2>
                            <div class=\"breadcrumb-wrapper col-12\">
                                <ol class=\"breadcrumb\">
                                    <li class=\"breadcrumb-item\"><a href=\"inicio.html\">Inicio</a>
                                    </li>
                                    <!-- <li class=\"breadcrumb-item\"><a href=\"#\">Card</a>
                                    </li>
                                    <li class=\"breadcrumb-item active\">Advance Card
                                    </li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"content-body\">
              <!-- Timeline Starts -->
              <section id=\"timeline-card\">
                  <div class=\"row\">
                    {% for a in alumnos %}
                        <div class=\"col-lg-6 col-sm-12\">
                            <div class=\"card\">

                                <div class=\"card-content\">
                                    <div class=\"card-body text-center\">
                                        <h4>{{a.nombre}} {{a.apellido}}</h4>
                                        <p class=\"\">{{a.curso != null ? a.curso.nombre : ''}}</p>
                                        <div class=\"card-btns d-flex justify-content-center\">
                                            <a class=\"btn btn-outline-primary\" href=\"/alumno/{{a.id}}\">Ver mas</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                  </div>
              </section>
              <!-- Timeline Ends -->






            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class=\"sidenav-overlay\"></div>
    <div class=\"drag-target\"></div>

    <!-- BEGIN: Footer-->
    <footer class=\"footer footer-static footer-light\">
        <p class=\"clearfix blue-grey lighten-2 mb-0\"><span class=\"float-md-left d-block d-md-inline-block mt-25\">Acricana - Cuaderno de Comunicados V0.1 - Powered by <a class=\"text-bold-800 grey darken-2\" href=\"#\" target=\"_blank\">Movius</a></span><span class=\"float-md-right d-none d-md-block\">Hand-crafted & Made with<i class=\"feather icon-heart pink\"></i></span>
            <button class=\"btn btn-primary btn-icon scroll-top\" type=\"button\"><i class=\"feather icon-arrow-up\"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src=\"../../../app-assets/vendors/js/vendors.min.js\"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src=\"../../../app-assets/js/core/app-menu.js\"></script>
    <script src=\"../../../app-assets/js/core/app.js\"></script>
    <script src=\"../../../app-assets/js/scripts/components.js\"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src=\"../../../app-assets/js/scripts/pages/app-chat.js\"></script>
    <script src=\"/firebase/index.js\"></script>
    <script src=\"/notify.js\"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
", "index-acricana.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/index-acricana.html.twig");
    }
}
