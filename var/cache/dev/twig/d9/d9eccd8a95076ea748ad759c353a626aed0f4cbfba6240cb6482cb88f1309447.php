<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* easy_admin/lecturas.html.twig */
class __TwigTemplate_dd1ef8eee7999d820ca9834f09ce91f4e3a4263b7ea6e9eedaa518bd3a49354d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'content_header' => [$this, 'block_content_header'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "easy_admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "easy_admin/lecturas.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "easy_admin/lecturas.html.twig"));

        $this->parent = $this->loadTemplate("easy_admin/layout.html.twig", "easy_admin/lecturas.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <section class=\"content-header\">
        ";
        // line 5
        $this->displayBlock('content_header', $context, $blocks);
        // line 8
        echo "    </section>

    <h4 style='margin-left: 15px'>Lecturas</h4>
    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 11, $this->source); })()), "lecturas", [], "any", false, false, false, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 12
            echo "        <div class=\"panel panel-primary\" style='margin: 15px'>
        <div class=\"panel-heading\">";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "usuario", [], "any", false, false, false, 13), "nombre", [], "any", false, false, false, 13), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "usuario", [], "any", false, false, false, 13), "apellido", [], "any", false, false, false, 13), "html", null, true);
            echo "</div>
        <div class=\"panel-body\">
            <ul class=\"list-group\">
                <li class=\"list-group-item\"><strong>Fecha: </strong> ";
            // line 16
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "fecha", [], "any", false, false, false, 16), "d/m/Y H:i"), "html", null, true);
            echo "</li>
                <li class=\"list-group-item\"><strong>Alumnos: </strong> ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "usuario", [], "any", false, false, false, 17), "alumnos", [], "any", false, false, false, 17));
            foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "nombre", [], "any", false, false, false, 17), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "apellido", [], "any", false, false, false, 17), "html", null, true);
                echo "<br/>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</li>
            </ul>
                
            
        </div>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        // line 6
        echo "            <h1 class=\"title\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["comunicado"]) || array_key_exists("comunicado", $context) ? $context["comunicado"] : (function () { throw new RuntimeError('Variable "comunicado" does not exist.', 6, $this->source); })()), "titulo", [], "any", false, false, false, 6), "html", null, true);
        echo "</h1>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "easy_admin/lecturas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 6,  133 => 5,  122 => 24,  98 => 17,  94 => 16,  86 => 13,  83 => 12,  79 => 11,  74 => 8,  72 => 5,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"easy_admin/layout.html.twig\" %}

{% block content %}
    <section class=\"content-header\">
        {% block content_header %}
            <h1 class=\"title\">{{comunicado.titulo}}</h1>
        {% endblock content_header %}
    </section>

    <h4 style='margin-left: 15px'>Lecturas</h4>
    {% for r in comunicado.lecturas %}
        <div class=\"panel panel-primary\" style='margin: 15px'>
        <div class=\"panel-heading\">{{r.usuario.nombre}} {{r.usuario.apellido}}</div>
        <div class=\"panel-body\">
            <ul class=\"list-group\">
                <li class=\"list-group-item\"><strong>Fecha: </strong> {{r.fecha|date('d/m/Y H:i')}}</li>
                <li class=\"list-group-item\"><strong>Alumnos: </strong> {% for a in r.usuario.alumnos %} {{a.nombre}} {{a.apellido}}<br/>{% endfor%}</li>
            </ul>
                
            
        </div>
    </div>
    {% endfor %}
    
{% endblock %}", "easy_admin/lecturas.html.twig", "/home/moises/Proyectos/cuaderno-comunicados-master(10)/cuaderno-comunicados-master/templates/easy_admin/lecturas.html.twig");
    }
}
